package com.farzain.carstore.parser;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.farzain.carstore.adapter.Item;
import com.farzain.carstore.model.CompanyModel;
import com.farzain.carstore.model.UserInfoModel;

public class JsonParser {
	public static final String TAG = JsonParser.class.getSimpleName();

	public static CompanyModel getCompanyHome(String jString) {
		CompanyModel cmpModel = null;
		JSONObject jsonObject = null;
		if (jString != null)
			try {
				jsonObject = new JSONObject(jString);

				String status = jsonObject.getString("status");
				if (status.equals("1")) {
					JSONArray jsonArray = jsonObject
							.getJSONArray("company_info");
					if (jsonArray.length() != 0) {
						JSONObject jObj = jsonArray.getJSONObject(0);
						cmpModel = new CompanyModel();
						cmpModel.setId(jObj.getString("id"));
						cmpModel.setComapnyAppId(jObj
								.getString("company_app_id"));
						cmpModel.setComapanyName(jObj
								.getString("_company_name"));
						cmpModel.setImageBanner(jObj
								.getString("homepage_banner"));
						cmpModel.setEmail(jObj.getString("email"));
						cmpModel.setCompanyCall(jObj.getString("company_call"));
						cmpModel.setRsAssistNumber(jObj
								.getString("_roadside_assitance"));
						cmpModel.setSplLink(jObj.getString("_special"));
						cmpModel.setLatitude(jObj.getString("latitude"));
						cmpModel.setLongitude(jObj.getString("longitude"));
						cmpModel.setSplLink(jObj.getString("_special"));
						cmpModel.setAddress(jObj.getString("_company_address"));
					}
				}

			} catch (JSONException e) {
				Log.e(TAG, e.getMessage());

			}

		return cmpModel;

	}

	public static CompanyModel getCompanyInstrument(String jString) {
		CompanyModel cmpModel = null;
		JSONObject jsonObject = null;
		try {
			jsonObject = new JSONObject(jString);

			String status = jsonObject.getString("status");
			if (status.equals("1")) {
				JSONArray jsonArray = jsonObject.getJSONArray("company_info");
				if (jsonArray.length() != 0) {
					JSONObject jObj = jsonArray.getJSONObject(0);
					cmpModel = new CompanyModel();
					cmpModel.setId(jObj.getString("id"));
					cmpModel.setComapnyAppId(jObj.getString("company_app_id"));
					cmpModel.setWebLinkCluster(jObj
							.getString("comp_instrumental_cluster"));
					cmpModel.setImageBanner(jObj.getString("_banner_culst"));
				}
			}

		} catch (JSONException e) {
			Log.e(TAG, e.getMessage());

		}

		return cmpModel;

	}

	public static List<Item> getListInstrument(String jString) {
		List<Item> listItem = null;
		JSONObject jsonObject = null;
		try {
			jsonObject = new JSONObject(jString);
			listItem = new ArrayList<Item>();
			String status = jsonObject.getString("status");
			if (status.equals("1")) {
				JSONArray jsonArray = jsonObject.getJSONArray("cluster_info");
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject obj = jsonArray.getJSONObject(i);
					Item item = new Item();
					item.setId(obj.getString("id"));
					item.setImageUrl(obj.getString("cluster_icon"));
					item.setCause(obj.getString("cause"));
					item.setAction(obj.getString("what_to_do"));
					listItem.add(item);
				}
			}

		} catch (JSONException e) {
			Log.e(TAG, e.getMessage());

		}

		return listItem;

	}

	public static CompanyModel getCompanyRequest(String jString) {
		CompanyModel cmpModel = null;
		JSONObject jsonObject = null;
		try {
			jsonObject = new JSONObject(jString);

			String status = jsonObject.getString("status");
			if (status.equals("1")) {
				JSONArray jsonArray = jsonObject.getJSONArray("company_info");
				if (jsonArray.length() != 0) {
					JSONObject jObj = jsonArray.getJSONObject(0);
					cmpModel = new CompanyModel();
					cmpModel.setId(jObj.getString("id"));
					cmpModel.setComapnyAppId(jObj.getString("company_app_id"));
					cmpModel.setComapanyName(jObj.getString("_company_name"));
					cmpModel.setImageBanner(jObj.getString("homepage_banner"));
					cmpModel.setRequestMsg(jObj
							.getString("request_service_content"));
				}
			}

		} catch (JSONException e) {
			Log.e(TAG, e.getMessage());

		}

		return cmpModel;

	}

	public static UserInfoModel getUserInfo(String jString) {

		UserInfoModel userModel = null;
		JSONObject jsonObject = null;
		try {
			if (jString != null) {

				jsonObject = new JSONObject(jString);

				String status = jsonObject.getString("status");
				if (status.equals("1")) {
					JSONArray jsonArray = jsonObject.getJSONArray("user_info");
					if (jsonArray.length() != 0) {
						JSONObject jObj = jsonArray.getJSONObject(0);
						userModel = new UserInfoModel();
						userModel.setId(jObj.getString("id"));
						userModel
								.setCompanyId(jObj.getString("company_app_id"));
						userModel.setFirstName(jObj.getString("first_name"));
						userModel.setLastName(jObj.getString("last_name"));
						userModel.setEmail(jObj.getString("email_address"));
						userModel.setPhoneHome(jObj.getString("phone_home"));
						userModel
								.setPhoneMobile(jObj.getString("phone_mobile"));
						userModel.setPhoneBusiness(jObj
								.getString("phone_bussiness"));
						userModel.setVehicleNum(jObj.getString("vehicle_info"));
						userModel.setServiceOptionStatus(jObj
								.getString("service_option"));
						userModel.setGeoFenceStatus(jObj
								.getString("geo_fences"));
						userModel.setServiceOptionRepair(jObj
								.getString("repair"));
					}
				}
			}

		} catch (JSONException e) {
			Log.e(TAG, e.getMessage());

		}

		return userModel;

	}
}
