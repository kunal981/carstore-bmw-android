package com.farzain.carstore.ws;

public class WSConstant {

	// public static final String API = "http://104.131.163.241";
	public static final String API = "http://108.167.189.25/~dealernx/dealershipportal";
	public static final String API_GET = API + "/?";
	public static final String API_GET_INSTRUMENT = API
			+ "/wp-admin/admin.php?";
	public static final String API_POST = API;
	public static final String METHOD_POST = "post";
	public static final String METHOD_GET = "get";

	public class PARAM {
		public static final String ACTION = "action";
		public static final String COMPANY_APP_ID = "company_app_id";
		public static final String DEVICE_TOKEN = "device_token";
		public static final String FIRST_NAME = "first_name";
		public static final String LAST_NAME = "last_name";
		public static final String EMAIL_ADDRESS = "email_address";
		public static final String PHONE_HOME = "phone_home";
		public static final String PHONE_MOBILE = "phone_mobile";
		public static final String PHONE_BUSSINESS = "phone_bussiness";
		public static final String VEHICLE_INFO = "vehicle_info";
		public static final String SERVICE_OPTION = "service_option";
		public static final String SERVICE_OPTION_TWO = "repair";
		public static final String GEO_FENCES = "geo_fences";
		public static final String PAGE = "page";
	}

	public class VALUES {
		public static final String ACTION_HOME = "show_home_data";
		public static final String ACTION_REQUEST_SERVICE = "show_request_ser";
		public static final String ACTION_INSTRUMENT_CLUSTER = "show_Inst_clstr";
		public static final String ACTION_INSTRUMENT_CLUSTER_DATA = "cluster_data";
		public static final String ACTION_USER_INFO = "show_user_info";
		public static final String ACTION_ADD_USER_INFO = "add_user_info";
		public static final String COMPANY_APP_ID = "BMW_716";
		public static final String DEVICE_TOKEN_VALUES = "234789uyxjh365u05769034y25234623fdgsdg";
		public static final String PAGE_VALUE = "car_app_api";
	}

}
