package com.farzain.carstore.ws;

import java.io.IOException;

import android.util.Log;

import com.farzain.carstore.model.CompanyModel;
import com.farzain.carstore.parser.JsonParser;
import com.farzain.carstore.ws.WSConstant.PARAM;
import com.farzain.carstore.ws.WSConstant.VALUES;

public class WSConnector {

	public static final String TAG = WSConnector.class.getSimpleName();

	public static CompanyModel getHomeInfo() {
		String url = null;
		url = WSConstant.API_GET + PARAM.ACTION + "=" + VALUES.ACTION_HOME
				+ "&" + PARAM.COMPANY_APP_ID + "=" + VALUES.COMPANY_APP_ID;
		String jString = null;
		try {
			jString = WSAdapter.getJSONObject(url);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return JsonParser.getCompanyHome(jString);

	}

}
