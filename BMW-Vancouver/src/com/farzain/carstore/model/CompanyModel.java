package com.farzain.carstore.model;

public class CompanyModel {

	private String id;
	private String comapnyAppId;
	private String comapanyName;
	private String imageBanner;
	private String email;
	private String companyCall;
	private String requestMsg;
	private String rsAssistNumber;
	private String webLinkCluster;
	private String splLink;
	private String address;
	private String latitude;
	private String longitude;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getComapnyAppId() {
		return comapnyAppId;
	}

	public void setComapnyAppId(String comapnyAppId) {
		this.comapnyAppId = comapnyAppId;
	}

	public String getComapanyName() {
		return comapanyName;
	}

	public void setComapanyName(String comapanyName) {
		this.comapanyName = comapanyName;
	}

	public String getImageBanner() {
		return imageBanner;
	}

	public void setImageBanner(String imageBanner) {
		this.imageBanner = imageBanner;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompanyCall() {
		return companyCall;
	}

	public void setCompanyCall(String companyCall) {
		this.companyCall = companyCall;
	}

	public String getRequestMsg() {
		return requestMsg;
	}

	public void setRequestMsg(String requestMsg) {
		this.requestMsg = requestMsg;
	}

	public String getRsAssistNumber() {
		return rsAssistNumber;
	}

	public void setRsAssistNumber(String rsAssistNumber) {
		this.rsAssistNumber = rsAssistNumber;
	}

	public String getSplLink() {
		return splLink;
	}

	public void setSplLink(String splLink) {
		this.splLink = splLink;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getWebLinkCluster() {
		return webLinkCluster;
	}

	public void setWebLinkCluster(String webLinkCluster) {
		this.webLinkCluster = webLinkCluster;
	}

}
