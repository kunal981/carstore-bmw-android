package com.farzain.carstore.model;

public class UserInfoModel {

	private String id;
	private String companyId;
	private String name;
	private String firstName;
	private String lastName;
	private String email;
	private String phoneHome;
	private String phoneMobile;
	private String phoneBusiness;
	private String vehicleNum;
	private String serviceOptionStatus;
	private String serviceOptionRepair;
	private String geoFenceStatus;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneHome() {
		return phoneHome;
	}

	public void setPhoneHome(String phoneHome) {
		this.phoneHome = phoneHome;
	}

	public String getPhoneMobile() {
		return phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}

	public String getPhoneBusiness() {
		return phoneBusiness;
	}

	public void setPhoneBusiness(String phoneBusiness) {
		this.phoneBusiness = phoneBusiness;
	}

	public String getVehicleNum() {
		return vehicleNum;
	}

	public void setVehicleNum(String vehicleNum) {
		this.vehicleNum = vehicleNum;
	}

	public String getServiceOptionStatus() {
		return serviceOptionStatus;
	}

	public void setServiceOptionStatus(String serviceOptionStatus) {
		this.serviceOptionStatus = serviceOptionStatus;
	}

	public String getGeoFenceStatus() {
		return geoFenceStatus;
	}

	public void setGeoFenceStatus(String geoFenceStatus) {
		this.geoFenceStatus = geoFenceStatus;
	}

	public String getServiceOptionRepair() {
		return serviceOptionRepair;
	}

	public void setServiceOptionRepair(String serviceOptionRepair) {
		this.serviceOptionRepair = serviceOptionRepair;
	}

}
