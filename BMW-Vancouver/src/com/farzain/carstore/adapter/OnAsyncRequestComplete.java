package com.farzain.carstore.adapter;

public interface OnAsyncRequestComplete {
	public void asyncResponse(int request, String response);
}
