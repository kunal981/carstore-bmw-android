package com.farzain.carstore.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.farzain.carstore.R;
import com.squareup.picasso.Picasso;

/**
 * 
 * @author
 * 
 */
public class CustomGridViewAdapter extends BaseAdapter {
	Context context;
	int layoutResourceId;
	List<Item> data;

	public CustomGridViewAdapter(Context context, int layoutResourceId,
			List<Item> data) {
		super();
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new RecordHolder();
			// holder.txtTitle = (TextView)
			// row.findViewById(R.id.item_textview);
			holder.imageItem = (ImageView) row
					.findViewById(R.id.item_imageview);

			row.setTag(holder);
		} else {
			holder = (RecordHolder) row.getTag();
		}

		Item item = data.get(position);
		// holder.txtTitle.setText(item.getTitle());
		// holder.imageItem.setImageBitmap(item.getImage());
		Picasso.with(context).load(item.getImageUrl()).into(holder.imageItem);
		return row;

	}

	static class RecordHolder {
		TextView txtTitle;
		ImageView imageItem;

	}

	public void update(List<Item> data) {
		this.data = data;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
}
