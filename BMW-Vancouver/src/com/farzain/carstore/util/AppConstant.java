package com.farzain.carstore.util;

public class AppConstant {

	public static final String PHONE_CAR_STORE = "6046593250";
	public static final String PHONE_ROAD_ASSIST = "18002678269";

	public static final String EMAIl_CAR_STORE = "service@thebmwstore.ca";

	/**
	 * not used currently
	 * 
	 * @author lenovo
	 * 
	 */
	public class UserInfoUtil {

		// The prefix for flattened geofence keys

		public static final String KEY_USER = "com.farzain.carstore.USER";
		public static final String KEY_PREFIX = "com.farzain.carstore.KEY";
		public static final String KEY_NAME = "com.farzain.carstore.KEY_NAME";
		public static final String KEY_VIN = "com.farzain.carstore.KEY_VIND";
		public static final String KEY_PHONE_HOME = "com.farzain.carstore.KEY_PHONE_HOME";
		public static final String KEY_PHONE_MOBILE = "com.farzain.carstore.KEY_PHONE_MOBILE";
		public static final String KEY_PHONE_BUSINESS = "com.farzain.carstore.KEY_PHONE_BUSINESS";
		public static final String KEY_PHONE_DROP_OFF = "com.farzain.carstore.KEY_PHONE_DROP_OFF";
		public static final String KEY_PHONE_REPAIR_COM = "com.farzain.carstore.KEY_PHONE_REPAIR_COM";

	}

}
