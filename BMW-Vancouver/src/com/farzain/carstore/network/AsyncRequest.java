package com.farzain.carstore.network;

import java.io.IOException;
import java.util.List;

import org.apache.http.NameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import com.farzain.carstore.adapter.OnAsyncRequestComplete;
import com.farzain.carstore.ws.WSAdapter;

public class AsyncRequest extends AsyncTask<String, Integer, String> {

	private static final String TAG = AsyncRequest.class.getSimpleName();

	OnAsyncRequestComplete caller;
	FragmentActivity context;
	String method = "GET";
	List<NameValuePair> parameters = null;
	ProgressDialog pDialog = null;
	int request;

	// Three Constructors
	public AsyncRequest(FragmentActivity a, OnAsyncRequestComplete sender,
			String m, List<NameValuePair> p) {
		caller = sender;
		context = a;
		method = m;
		parameters = p;
	}

	public AsyncRequest(FragmentActivity a, OnAsyncRequestComplete sender,
			String m) {
		caller = sender;
		context = a;
		method = m;
	}

	public AsyncRequest(FragmentActivity a, OnAsyncRequestComplete sender) {
		caller = sender;
		context = a;
	}

	public AsyncRequest(FragmentActivity a, OnAsyncRequestComplete sender,
			String m, int serviceRequestGetUser) {
		caller = sender;
		context = a;
		method = m;
		request = serviceRequestGetUser;
	}

	@Override
	protected String doInBackground(String... params) {

		String address = params[0].toString();
		if (method == "POST") {
			String result = null;
			try {
				result = WSAdapter.postJSONObject(address, parameters);
			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
				e.printStackTrace();
			}
			return result;
		}

		if (method == "GET") {
			String result = null;
			try {
				result = WSAdapter.getJSONObject(address);
			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
				e.printStackTrace();
			}
			return result;
		}

		return null;
	}

	public void onPreExecute() {
		pDialog = new ProgressDialog(context);
		pDialog.setMessage("Loading data.."); // typically you will define such
		// strings in a remote file.
		pDialog.show();
	}

	public void onProgressUpdate(Integer... progress) {
		// you can implement some progressBar and update it in this record
		// setProgressPercent(progress[0]);
	}

	public void onPostExecute(String response) {
		if (pDialog != null && pDialog.isShowing()) {
			pDialog.dismiss();
		}

		caller.asyncResponse(request, response);
	}

	protected void onCancelled(String response) {
		if (pDialog != null && pDialog.isShowing()) {
			pDialog.dismiss();
		}
		caller.asyncResponse(request, response);
	}
}
