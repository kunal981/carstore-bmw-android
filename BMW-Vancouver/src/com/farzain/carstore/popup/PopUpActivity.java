package com.farzain.carstore.popup;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.farzain.carstore.R;
import com.farzain.carstore.adapter.Item;
import com.farzain.carstore.fragment.instrument.InstrumentClusterFragment;
import com.farzain.carstore.model.CompanyModel;
import com.farzain.carstore.model.UserInfoModel;
import com.farzain.carstore.parser.JsonParser;
import com.farzain.carstore.pref.UserStore;
import com.farzain.carstore.util.Log;
import com.farzain.carstore.util.AppConstant.UserInfoUtil;
import com.squareup.picasso.Picasso;

public class PopUpActivity extends Activity {

	Button mButton_close;
	public String companyNumber;
	public String companyEmail;
	Button buttonCall, buttonEmail;
	ImageView mImageView;
	TextView mTextViewSimple, mTextViewLast;
	Integer mInteger;
	private UserStore userPrefs;
	String imagePath, cause, action;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup);
		userPrefs = new UserStore(this);

		mImageView = (ImageView) findViewById(R.id.show_imageview);
		mTextViewSimple = (TextView) findViewById(R.id.simple_text);
		mTextViewLast = (TextView) findViewById(R.id.last_text);
		buttonEmail = (Button) findViewById(R.id.button_email);
		buttonCall = (Button) findViewById(R.id.button_call);
		imagePath = getIntent().getStringExtra("IMAGEPATH");
		cause = getIntent().getStringExtra("CAUSE");
		action = getIntent().getStringExtra("ACTION");
		mTextViewSimple.setText(cause);
		mTextViewLast.setText(action);
		// holder.txtTitle.setText(item.getTitle());
		Picasso.with(this).load(imagePath).into(mImageView);

		mButton_close = (Button) findViewById(R.id.button_close);
		CompanyModel model = new CompanyModel();
		if (model != null) {
			companyNumber = model.getCompanyCall();
			companyEmail = model.getEmail();
		}
		mButton_close.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		buttonCall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Toast.makeText(getApplicationContext(), "Calling",
						Toast.LENGTH_SHORT).show();
				// callService(AppConstant.PHONE_CAR_STORE);
				callService(companyNumber);

			}
		});
		buttonEmail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Toast.makeText(getApplicationContext(), "Email",
						Toast.LENGTH_SHORT).show();
				emailService();

			}
		});

	}

	public void callService(String number) {

		String uri2 = "tel:" + number;
		Intent intent2 = new Intent(Intent.ACTION_CALL);
		intent2.setData(Uri.parse(uri2));
		startActivity(intent2);
	}

	protected void emailService() {

		UserInfoModel user = userPrefs.getUserInfo(UserInfoUtil.KEY_USER);
		if (!user.getVehicleNum().equals("")) {

			String emailTemplete = String.valueOf("VIN: "
					+ user.getVehicleNum() + "+\nCustomer Name: "
					+ user.getName() + "\nCustomer phone number(HOME): "
					+ user.getPhoneHome() + "\nCustomer phone number(MOBILE): "
					+ user.getPhoneMobile()
					+ "\nCustomer phone number(BUSINESS): "
					+ user.getPhoneBusiness() + "\nDrop off required:  "
					+ user.getServiceOptionStatus()
					+ "\nRepair completion text required: "
					+ user.getServiceOptionRepair());

			Intent email = new Intent(Intent.ACTION_SEND);
			// email.putExtra(Intent.EXTRA_EMAIL,
			// new String[] { getString(R.string.text_email_to) });
			email.putExtra(Intent.EXTRA_EMAIL, new String[] { companyEmail });
			// email.putExtra(Intent.EXTRA_CC, new String[]{ to});
			email.putExtra(Intent.EXTRA_BCC,
					new String[] { getString(R.string.text_email_bcc) });
			email.putExtra(Intent.EXTRA_SUBJECT,
					getString(R.string.text_email_sub));

			email.putExtra(Intent.EXTRA_TEXT, emailTemplete);

			// need this to prompts email client only
			email.setType("message/rfc822");

			startActivity(Intent.createChooser(email,
					"Choose an Email client :"));

		} else {
			// Toast.makeText(getApplicationContext(),
			// "Please setup your user detail on setting page first",
			// Toast.LENGTH_SHORT)
			// .show();
			showAlertDialog(PopUpActivity.this, "User Detail",
					"Please setup your user detail on setting page first",
					false);
		}

	}

	public void showAlertDialog(Context context, String title, String message,
			boolean error) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);
		if (error)
			alertDialogBuilder.setIcon(R.drawable.fail);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						dialog.cancel();

					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public Bitmap StringToBitMap(String encodedString) {
		try {
			byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
			Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0,
					encodeByte.length);
			return bitmap;
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

}
