package com.farzain.carstore.fragment.special;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.farzain.carstore.R;
import com.farzain.carstore.controller.UiHandler;
import com.farzain.carstore.model.CompanyModel;

public class SpecialFragment extends Fragment {

	private static final String ARG_SECTION_NUMBER = "section_number";

	WebView myWebView;
	UiHandler uHandler;
	CompanyModel cmpModel;

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static SpecialFragment newInstance(int sectionNumber) {
		SpecialFragment fragment = new SpecialFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uHandler = UiHandler.getInstance();
		cmpModel = uHandler.getCompanyInfo();
	}

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_layout_special,
				container, false);
		myWebView = (WebView) rootView.findViewById(R.id.webview);
		myWebView.getSettings().setJavaScriptEnabled(true);

		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();
		setupWebView();
	}

	private class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}

	private void setupWebView() {
		myWebView.getSettings().setBuiltInZoomControls(true);
		myWebView.getSettings().setLoadWithOverviewMode(true);
		myWebView.getSettings().setUseWideViewPort(true);
		myWebView.setWebViewClient(new MyWebViewClient());
		myWebView.loadUrl(cmpModel.getSplLink());
	}

	@Override
	public void onPause() {
		super.onPause();
		myWebView.getSettings().setBuiltInZoomControls(false);
		myWebView.getSettings().setLoadWithOverviewMode(false);
		myWebView.getSettings().setUseWideViewPort(false);
	}

	// private int getScale() {
	// DisplayMetrics displaymetrics = new DisplayMetrics();
	// getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
	// int screenWidth = displaymetrics.widthPixels;
	// int screenHeight = displaymetrics.heightPixels;
	//
	// Display display = ((WindowManager) getActivity().getSystemService(
	// Context.WINDOW_SERVICE)).getDefaultDisplay();
	// int width = display.getWidth();
	// Double val = new Double(width) / new Double(PIC_WIDTH);
	// val = val * 100d;
	// return val.intValue();
	// }

}
