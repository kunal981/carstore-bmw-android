package com.farzain.carstore.fragment.home;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.farzain.carstore.R;
import com.farzain.carstore.adapter.OnAsyncRequestComplete;
import com.farzain.carstore.controller.UiHandler;
import com.farzain.carstore.model.CompanyModel;
import com.farzain.carstore.model.UserInfoModel;
import com.farzain.carstore.network.AsyncRequest;
import com.farzain.carstore.parser.JsonParser;
import com.farzain.carstore.pref.UserStore;
import com.farzain.carstore.util.AppConstant;
import com.farzain.carstore.util.AppConstant.UserInfoUtil;
import com.farzain.carstore.util.Log;
import com.farzain.carstore.ws.WSConstant;
import com.farzain.carstore.ws.WSConstant.PARAM;
import com.farzain.carstore.ws.WSConstant.VALUES;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

public class HomeFragment extends Fragment implements OnAsyncRequestComplete {

	/**
	 * The fragment argument representing the section number for this fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";
	public String title = "BMW Store Vancouver";
	public String address = "1740 W 5th Ave.Vancouver B.C. V6J 1P2";

	public String companyNumber;
	public String companyEmail;
	public String roadAssitanceNumber;

	private GoogleMap mMap;
	private MapView mMapView;
	private Marker marker;

	LatLng latLng = new LatLng(53.540101, -113.651181);
	
	View rootView;
	Button buttonCall, buttonEmail, buttonRoadAssist;
	ImageView imageBanner;

	private UserStore userPrefs;

	UiHandler uHandler;

	OnAsyncRequestComplete sender;


	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static HomeFragment newInstance(int sectionNumber) {
		HomeFragment fragment = new HomeFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uHandler = UiHandler.getInstance();
		userPrefs = new UserStore(getActivity());

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_layout_home,
					container, false);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}

		imageBanner = (ImageView) rootView.findViewById(R.id.img_home_banner);
		buttonCall = (Button) rootView.findViewById(R.id.button_call);
		buttonRoadAssist = (Button) rootView
				.findViewById(R.id.button_road_assist);

		buttonEmail = (Button) rootView.findViewById(R.id.button_email);

		MapsInitializer.initialize(getActivity());
		mMapView = (MapView) rootView.findViewById(R.id.map);
		mMapView.onCreate(savedInstanceState);

		setUpMapAndUserCurrentLocation();

		executeHomeNetworkTask();

		return rootView;
	}

	private void executeHomeNetworkTask() {
		String url = null;
		url = WSConstant.API_GET + PARAM.ACTION + "=" + VALUES.ACTION_HOME
				+ "&" + PARAM.COMPANY_APP_ID + "=" + VALUES.COMPANY_APP_ID;
		sender = (OnAsyncRequestComplete) this;
		AsyncRequest getHomeInfoTask = new AsyncRequest(getActivity(), sender,
				"GET");
		getHomeInfoTask.execute(url);
	}

	private void addClickListenerButton() {

		buttonCall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Toast.makeText(getActivity(), "Calling", Toast.LENGTH_SHORT)
						.show();
				// callService(AppConstant.PHONE_CAR_STORE);
				callService(companyNumber);

			}
		});
		buttonEmail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Toast.makeText(getActivity(), "Email", Toast.LENGTH_SHORT)
						.show();
				emailService();

			}
		});
		buttonRoadAssist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// callService(AppConstant.PHONE_ROAD_ASSIST);
				callService(roadAssitanceNumber);

			}
		});

		imageBanner.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// callService(AppConstant.PHONE_CAR_STORE);
				callService(companyNumber);
			}
		});

	}

	protected void emailService() {

		UserInfoModel user = userPrefs.getUserInfo(UserInfoUtil.KEY_USER);
		if (!user.getVehicleNum().equals("")) {

			String emailTemplete = String.valueOf("VIN: "
					+ user.getVehicleNum() + "+\nCustomer Name: "
					+ user.getName() + "\nCustomer phone number(HOME): "
					+ user.getPhoneHome() + "\nCustomer phone number(MOBILE): "
					+ user.getPhoneMobile()
					+ "\nCustomer phone number(BUSINESS): "
					+ user.getPhoneBusiness() + "\nDrop off required:  "
					+ user.getServiceOptionStatus()
					+ "\nRepair completion text required: "
					+ user.getServiceOptionRepair());

			Intent email = new Intent(Intent.ACTION_SEND);
			// email.putExtra(Intent.EXTRA_EMAIL,
			// new String[] { getString(R.string.text_email_to) });
			email.putExtra(Intent.EXTRA_EMAIL, new String[] { companyEmail });
			// email.putExtra(Intent.EXTRA_CC, new String[]{ to});
			email.putExtra(Intent.EXTRA_BCC,
					new String[] { getString(R.string.text_email_bcc) });
			email.putExtra(Intent.EXTRA_SUBJECT,
					getString(R.string.text_email_sub));

			email.putExtra(Intent.EXTRA_TEXT, emailTemplete);

			// need this to prompts email client only
			email.setType("message/rfc822");

			startActivity(Intent.createChooser(email,
					"Choose an Email client :"));

		} else {
			Log.showAlertDialog(getActivity(), "User Detail",
					"Please setup your user detail on setting page first",
					false);
		}

	}

	public void callService(String number) {

		String uri2 = "tel:" + number;
		Intent intent2 = new Intent(Intent.ACTION_CALL);
		intent2.setData(Uri.parse(uri2));
		startActivity(intent2);
	}

	private void updateMapElement(CompanyModel model) {
		LatLng clatLng = null;
		if (model.getLatitude() != null && !model.getLatitude().equals("")
				&& model.getLongitude() != null
				&& !model.getLongitude().equals("")) {
			double lat = Double.parseDouble(model.getLatitude());
			double lng = Double.parseDouble(model.getLongitude());
			clatLng = new LatLng(lat, lng);
		}

		if (mMap != null)
			if (clatLng != null) {
				marker = mMap.addMarker(new MarkerOptions()
						.position(clatLng)
						.title(model.getComapanyName())
						.snippet(
								String.valueOf(model.getAddress()
										+ "\n Contact: "
										+ model.getCompanyCall()))
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.ic_map)));
				mMap.moveCamera(CameraUpdateFactory.newLatLng(clatLng));
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
						clatLng, 15);
				mMap.animateCamera(cameraUpdate);

			}

	}

	private void setUpMapAndUserCurrentLocation() {
		if (mMap == null) {
			mMap = mMapView.getMap();
			mMap.getUiSettings().setMyLocationButtonEnabled(false);
			mMap.setMyLocationEnabled(true);
			LatLng clatLng = null;
			Location loc = mMap.getMyLocation();
			if (loc != null) {
				clatLng = new LatLng(loc.getLatitude(), loc.getLongitude());
			} else {
				clatLng = new LatLng(49.2756d, -123.12717d);
			}
			mMap.moveCamera(CameraUpdateFactory.newLatLng(clatLng));
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
					clatLng, 12);
			mMap.animateCamera(cameraUpdate);
		}

	}

	@Override
	public void onResume() {
		super.onResume();
		mMapView.onResume();
	}

	@Override
	public void onPause() {
		mMapView.onPause();
		super.onPause();
		mMap = null;
	}

	@Override
	public void onDestroy() {
		mMapView.onDestroy();
		super.onDestroy();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mMapView.onLowMemory();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mMapView.onSaveInstanceState(outState);
	}

	public void updatUiItem(CompanyModel model) {

		Picasso.with(getActivity()).load(model.getImageBanner())
				.resize(400, 200).centerCrop().into(imageBanner);

		companyNumber = model.getCompanyCall();
		companyEmail = model.getEmail();
		roadAssitanceNumber = model.getRsAssistNumber();

		if (model.getLatitude() != null && model.getLatitude().equals("")
				&& model.getLongitude() != null
				&& model.getLongitude().equals("")) {
			model.setLatitude(String.valueOf(latLng.latitude));
			model.setLongitude(String.valueOf(latLng.longitude));
		}
		uHandler.setCompanyInfo(model);
		addClickListenerButton();
		updateMapElement(model);
	}

	@Override
	public void asyncResponse(int request, String response) {
		CompanyModel model = JsonParser.getCompanyHome(response);
		if (model != null) {
			updatUiItem(model);
		}
	}
}
