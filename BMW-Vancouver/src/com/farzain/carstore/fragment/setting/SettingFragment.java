package com.farzain.carstore.fragment.setting;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;
import com.farzain.carstore.R;
import com.farzain.carstore.adapter.OnAsyncRequestComplete;
import com.farzain.carstore.controller.UiHandler;
import com.farzain.carstore.geofence.GeofenceRemover;
import com.farzain.carstore.geofence.GeofenceRequester;
import com.farzain.carstore.geofence.GeofenceUtils;
import com.farzain.carstore.geofence.GeofenceUtils.REMOVE_TYPE;
import com.farzain.carstore.geofence.GeofenceUtils.REQUEST_TYPE;
import com.farzain.carstore.geofence.SimpleGeofence;
import com.farzain.carstore.geofence.SimpleGeofenceStore;
import com.farzain.carstore.model.UserInfoModel;
import com.farzain.carstore.network.AsyncRequest;
import com.farzain.carstore.parser.JsonParser;
import com.farzain.carstore.pref.UserStore;
import com.farzain.carstore.util.AppConstant.UserInfoUtil;
import com.farzain.carstore.ws.WSConstant;
import com.farzain.carstore.ws.WSConstant.PARAM;
import com.farzain.carstore.ws.WSConstant.VALUES;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.maps.model.LatLng;

public class SettingFragment extends Fragment implements OnAsyncRequestComplete {

	public static final String TAG = SettingFragment.class.getSimpleName();

	private static final int ZBAR_SCANNER_REQUEST = 0;
	private static final int ZBAR_QR_SCANNER_REQUEST = 1;
	private static final int SERVICE_REQUEST_GET_USER = 328;
	private static final int SERVICE_REQUEST_GET_UPDATE = 329;

	private static final long GEOFENCE_EXPIRATION_IN_HOURS = 12;
	private static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = GEOFENCE_EXPIRATION_IN_HOURS
			* DateUtils.HOUR_IN_MILLIS;
	// private static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = 90000;

	// public LatLng latLng = new LatLng(30.6968559, 76.8538333);
	public LatLng latLng = new LatLng(51.04882, -114.06825);

	public float radius = 100;

	// Store the current request
	private REQUEST_TYPE mRequestType;

	// Store the current type of removal
	private REMOVE_TYPE mRemoveType;

	// Persistent storage for geofences
	private SimpleGeofenceStore mPrefs;
	private UserStore userPrefs;

	private SimpleGeofence mUIGeofence2;

	// decimal formats for latitude, longitude, and radius
	private DecimalFormat mLatLngFormat;
	private DecimalFormat mRadiusFormat;

	/*
	 * An instance of an inner class that receives broadcasts from listeners and
	 * from the IntentService that receives geofence transition events
	 */
	private GeofenceSampleReceiver mBroadcastReceiver;

	// An intent filter for the broadcast receiver
	private IntentFilter mIntentFilter;

	// Store a list of geofences to add
	List<Geofence> mCurrentGeofences;

	// Add geofences handler
	private GeofenceRequester mGeofenceRequester;
	// Remove geofences handler
	private GeofenceRemover mGeofenceRemover;
	// Handle to geofence 1 latitude in the UI

	// Store the list of geofences to remove
	private List<String> mGeofenceIdsToRemove;

	/**
	 * The fragment argument representing the section number for this fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";
	private String android_id;

	Button buttonScan, buttonSave;
	EditText editVechileNumber;
	EditText firstName, lastName, email, phoneHome, phoneMobile, phoneBusiness;

	ToggleButton toogleServiceOne, toogleServiceGeoCoding;
	// toogleServiceTwo
	String geoFenceStatus;
	String serviceStatus;
	String serviceStatusRepair;

	UiHandler uHandler;

	String userFirstName, userLastName, userEmail, userPhoneHome,
			userPhoneMoblie, userPhoneBusiness, userVechileNum,
			userGeoFenchStatus;

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static SettingFragment newInstance(int sectionNumber) {
		SettingFragment fragment = new SettingFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		uHandler = UiHandler.getInstance();
		android_id = Secure.getString(getActivity().getContentResolver(),
				Secure.ANDROID_ID);
		geoFenceStatus = "no";
		serviceStatus = "no";

		// Set the pattern for the latitude and longitude format
		String latLngPattern = getString(R.string.lat_lng_pattern);

		// Set the format for latitude and longitude
		mLatLngFormat = new DecimalFormat(latLngPattern);

		// Localize the format
		mLatLngFormat.applyLocalizedPattern(mLatLngFormat.toLocalizedPattern());

		// Set the pattern for the radius format
		String radiusPattern = getString(R.string.radius_pattern);

		// Set the format for the radius
		mRadiusFormat = new DecimalFormat(radiusPattern);

		// Localize the pattern
		mRadiusFormat.applyLocalizedPattern(mRadiusFormat.toLocalizedPattern());

		// Create a new broadcast receiver to receive updates from the listeners
		// and service
		mBroadcastReceiver = new GeofenceSampleReceiver();

		// Create an intent filter for the broadcast receiver
		mIntentFilter = new IntentFilter();

		// Action for broadcast Intents that report successful addition of
		// geofences
		mIntentFilter.addAction(GeofenceUtils.ACTION_GEOFENCES_ADDED);

		// Action for broadcast Intents that report successful removal of
		// geofences
		mIntentFilter.addAction(GeofenceUtils.ACTION_GEOFENCES_REMOVED);

		// Action for broadcast Intents containing various types of geofencing
		// errors
		mIntentFilter.addAction(GeofenceUtils.ACTION_GEOFENCE_ERROR);

		// All Location Services sample apps use this category
		mIntentFilter.addCategory(GeofenceUtils.CATEGORY_LOCATION_SERVICES);

		// Instantiate a new geofence storage area
		mPrefs = new SimpleGeofenceStore(getActivity());

		userPrefs = new UserStore(getActivity());

		// Instantiate the current List of geofences
		mCurrentGeofences = new ArrayList<Geofence>();

		// Instantiate a Geofence requester
		mGeofenceRequester = new GeofenceRequester(getActivity());

		// Instantiate a Geofence remover
		mGeofenceRemover = new GeofenceRemover(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_layout_setting,
				container, false);

		editVechileNumber = (EditText) rootView
				.findViewById(R.id.edit_setting_vechile_info);
		firstName = (EditText) rootView.findViewById(R.id.edit_setting_fn);
		lastName = (EditText) rootView.findViewById(R.id.edit_setting_ln);
		email = (EditText) rootView.findViewById(R.id.edit_setting_email);
		phoneHome = (EditText) rootView
				.findViewById(R.id.edit_setting_phone_home);
		phoneMobile = (EditText) rootView
				.findViewById(R.id.edit_setting_phone_mobile);
		phoneBusiness = (EditText) rootView
				.findViewById(R.id.edit_setting_phone_buisness);

		buttonScan = (Button) rootView.findViewById(R.id.btn_scan);
		buttonSave = (Button) rootView.findViewById(R.id.btn_save);

		toogleServiceOne = (ToggleButton) rootView
				.findViewById(R.id.toggleButton1);
		// toogleServiceTwo = (ToggleButton) rootView
		// .findViewById(R.id.toggleButton2);
		toogleServiceGeoCoding = (ToggleButton) rootView
				.findViewById(R.id.toggleButton_geo_fence);

		addToggleClickListener();

		addButtonListener();
		showUserInfoTask();

		return rootView;
	}

	private void addButtonListener() {
		// TODO Auto-generated method stub
		buttonScan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				launchScanner(v);
			}
		});
		buttonSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				updateUserInfo();
			}
		});

	}

	private void showUserInfoTask() {
		String url = null;
		url = WSConstant.API_GET + PARAM.ACTION + "=" + VALUES.ACTION_USER_INFO
				+ "&" + PARAM.COMPANY_APP_ID + "=" + VALUES.COMPANY_APP_ID
				+ "&" + PARAM.DEVICE_TOKEN + "=" + android_id;
		AsyncRequest getHomeInfoTask = new AsyncRequest(getActivity(), this,
				"GET", SERVICE_REQUEST_GET_USER);
		getHomeInfoTask.execute(url);
	}

	private void updateUserToNetwork() {
		String url = null;
		url = WSConstant.API_GET + PARAM.ACTION + "="
				+ VALUES.ACTION_ADD_USER_INFO + "&" + PARAM.COMPANY_APP_ID
				+ "=" + VALUES.COMPANY_APP_ID + "&" + PARAM.DEVICE_TOKEN + "="
				+ android_id + "&" + PARAM.FIRST_NAME + "=" + userFirstName
				+ "&" + PARAM.LAST_NAME + "=" + userLastName + "&"
				+ PARAM.EMAIL_ADDRESS + "=" + userEmail + "&"
				+ PARAM.PHONE_HOME + "=" + userPhoneHome + "&"
				+ PARAM.PHONE_BUSSINESS + "=" + userPhoneBusiness + "&"
				+ PARAM.PHONE_MOBILE + "=" + userPhoneMoblie + "&"
				+ PARAM.VEHICLE_INFO + "=" + userVechileNum + "&"
				+ PARAM.SERVICE_OPTION + "=" + serviceStatus + "&"
				+ PARAM.GEO_FENCES + "=" + geoFenceStatus + "&"
				+ PARAM.SERVICE_OPTION_TWO + "=" + serviceStatusRepair;
		AsyncRequest getHomeInfoTask = new AsyncRequest(getActivity(), this,
				"GET", SERVICE_REQUEST_GET_UPDATE);
		getHomeInfoTask.execute(url);
	}

	private void updateUiElement(UserInfoModel model) {
		firstName.setText(model.getFirstName());
		lastName.setText(model.getLastName());
		email.setText(model.getEmail());
		phoneHome.setText(model.getPhoneHome());
		phoneMobile.setText(model.getPhoneMobile());
		phoneBusiness.setText(model.getPhoneBusiness());
		editVechileNumber.setText(model.getVehicleNum());
		if (model.getGeoFenceStatus().toLowerCase(Locale.getDefault())
				.equals("yes")) {
			toogleServiceGeoCoding.setChecked(true);
		} else {
			toogleServiceGeoCoding.setChecked(false);
		}
		if (model.getServiceOptionStatus().toLowerCase(Locale.getDefault())
				.equals("yes")) {
			toogleServiceOne.setChecked(true);
		} else {
			toogleServiceOne.setChecked(false);
		}
		if (model.getServiceOptionRepair().toLowerCase(Locale.getDefault())
				.equals("yes")) {
			// toogleServiceTwo.setChecked(true);
		} else {
			// toogleServiceTwo.setChecked(false);
		}

	}

	protected void updateUserInfo() {

		if (isUserValid()) {
			updateUserToNetwork();
		}

	}

	private boolean isUserValid() {

		if (!isPersonalInfoValid()) {
			return false;
		}
		if (!isEmail()) {
			return false;
		}
		if (!isPhoneValid()) {
			return false;
		}
		if (!isValidVechile()) {
			return false;
		}

		return true;
	}

	private boolean isPersonalInfoValid() {
		userFirstName = firstName.getText().toString();
		userLastName = lastName.getText().toString();
		boolean status;
		if (userFirstName != null && !userFirstName.trim().equals("")) {
			status = true;
		} else {
			firstName.setError("Please enter first name");
			return false;

		}
		if (userLastName != null && !userLastName.trim().equals("")) {
			status = true;
		} else {
			lastName.setError("Please enter last name");
			return false;

		}
		return status;
	}

	private boolean isEmail() {
		userEmail = email.getText().toString();
		boolean status;
		if (userEmail != null && !userEmail.trim().equals("")) {
			status = true;
		} else {
			email.setError("Please enter email");
			return false;

		}
		return status;
	}

	private boolean isPhoneValid() {
		userPhoneHome = phoneHome.getText().toString();
		userPhoneMoblie = phoneMobile.getText().toString();
		userPhoneBusiness = phoneBusiness.getText().toString();
		boolean status;
		if ((userPhoneHome != null && !userPhoneHome.trim().equals(""))
				|| (userPhoneHome != null && !userPhoneHome.trim().equals(""))
				|| (userPhoneHome != null && !userPhoneHome.trim().equals(""))) {
			status = true;
		} else {
			phoneHome.setError("Please enter alteast one phone number");
			return false;

		}
		return status;
	}

	private boolean isValidVechile() {
		userVechileNum = editVechileNumber.getText().toString();
		boolean status;
		if (userVechileNum != null && !userVechileNum.trim().equals("")) {
			status = true;
		} else {
			editVechileNumber.setError("Please enter vechile number");
			return false;

		}
		return status;
	}

	@Override
	public void onResume() {
		super.onResume();
		// Register the broadcast receiver to receive status updates
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				mBroadcastReceiver, mIntentFilter);
		/*
		 * Get existing geofences from the latitude, longitude, and radius
		 * values stored in SharedPreferences. If no values exist, null is
		 * returned.
		 */
		mUIGeofence2 = mPrefs.getGeofence("2");
		/*
		 * If the returned geofences have values, use them to set values in the
		 * UI, using the previously-defined number formats.
		 */

		if (mUIGeofence2 != null) {
			Log.v(TAG, mUIGeofence2.toString());
			// toogleServiceGeoCoding.setChecked(true);

		}
	}

	/*
	 * Save the current geofence settings in SharedPreferences.
	 */
	@Override
	public void onPause() {
		super.onPause();
		if (mUIGeofence2 != null) {
			mPrefs.setGeofence("2", mUIGeofence2);
		}
	}

	private void addToggleClickListener() {
		toogleServiceOne
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean isChecked) {
						if (isChecked) {
							// Toast.makeText(getActivity(),
							// "Registered for service" + isChecked,
							// Toast.LENGTH_SHORT).show();
							serviceStatus = "yes";

						} else {
							// Toast.makeText(
							// getActivity(),
							// "Subscription ended for service"
							// + isChecked, Toast.LENGTH_SHORT)
							// .show();
							serviceStatus = "no";
						}
					}
				});
		// toogleServiceTwo
		// .setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//
		// @Override
		// public void onCheckedChanged(CompoundButton arg0,
		// boolean isChecked) {
		// if (isChecked) {
		// // Toast.makeText(getActivity(),
		// // "Registered for service",
		// // Toast.LENGTH_SHORT).show();
		// serviceStatusRepair = "yes";
		//
		// } else {
		// // Toast.makeText(getActivity(),
		// // "Subscription ended for service",
		// // Toast.LENGTH_SHORT).show();
		// serviceStatusRepair = "no";
		// }
		// }
		// });
		toogleServiceGeoCoding
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean isChecked) {
						if (isChecked) {
							geoFenceStatus = "yes";

						} else {
							geoFenceStatus = "no";
						}
					}
				});
		toogleServiceGeoCoding.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (geoFenceStatus.equalsIgnoreCase("yes")) {
					Toast.makeText(getActivity(), "Geofencing service start",
							Toast.LENGTH_SHORT).show();
					onRegisterClicked();
				} else if (geoFenceStatus.equalsIgnoreCase("no")) {
					Toast.makeText(getActivity(), "Geofence: service stopped",
							Toast.LENGTH_SHORT).show();
					onUnregisterGeofence2Clicked();
				}

			}
		});
	}

	public void launchScanner(View v) {
		if (isCameraAvailable()) {
			Intent intent = new Intent(getActivity(), ZBarScannerActivity.class);
			startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
		} else {
			Toast.makeText(getActivity(), "Rear Facing Camera Unavailable",
					Toast.LENGTH_SHORT).show();
		}
	}

	public boolean isCameraAvailable() {
		PackageManager pm = getActivity().getPackageManager();
		return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case ZBAR_SCANNER_REQUEST:
			if (resultCode == Activity.RESULT_OK) {
				// Toast.makeText(
				// getActivity(),
				// "Scan Result = "
				// + data.getStringExtra(ZBarConstants.SCAN_RESULT),
				// Toast.LENGTH_SHORT).show();
				if (data != null) {
					String vinNumber = data
							.getStringExtra(ZBarConstants.SCAN_RESULT);
					editVechileNumber.setText(vinNumber);
				}

			} else if (resultCode == Activity.RESULT_CANCELED && data != null) {
				String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
				if (!TextUtils.isEmpty(error)) {
					Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT)
							.show();
				}
			}
			break;
		// If the request code matches the code sent in onConnectionFailed
		case GeofenceUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST:

			switch (resultCode) {
			// If Google Play services resolved the problem
			case Activity.RESULT_OK:

				// If the request was to add geofences
				if (GeofenceUtils.REQUEST_TYPE.ADD == mRequestType) {

					// Toggle the request flag and send a new request
					mGeofenceRequester.setInProgressFlag(false);

					// Restart the process of adding the current geofences
					mGeofenceRequester.addGeofences(mCurrentGeofences);

					// If the request was to remove geofences
				} else if (GeofenceUtils.REQUEST_TYPE.REMOVE == mRequestType) {

					// Toggle the removal flag and send a new removal request
					mGeofenceRemover.setInProgressFlag(false);

					// If the removal was by Intent
					if (GeofenceUtils.REMOVE_TYPE.INTENT == mRemoveType) {

						// Restart the removal of all geofences for the
						// PendingIntent
						mGeofenceRemover
								.removeGeofencesByIntent(mGeofenceRequester
										.getRequestPendingIntent());

						// If the removal was by a List of geofence IDs
					} else {

						// Restart the removal of the geofence list
						mGeofenceRemover
								.removeGeofencesById(mGeofenceIdsToRemove);
					}
				}
				break;

			// If any other result was returned by Google Play services
			default:

				// Report that Google Play services was unable to resolve the
				// problem.
				Log.d(GeofenceUtils.APPTAG, getString(R.string.no_resolution));
			}

			// If any other request code was received
		default:
			// Report that this Activity received an unknown requestCode
			Log.d(GeofenceUtils.APPTAG,
					getString(R.string.unknown_activity_request_code,
							requestCode));

			break;

		}
	}

	/**
	 * Define a Broadcast receiver that receives updates from connection
	 * listeners and the geofence transition service.
	 */
	public class GeofenceSampleReceiver extends BroadcastReceiver {
		/*
		 * Define the required method for broadcast receivers This method is
		 * invoked when a broadcast Intent triggers the receiver
		 */
		@Override
		public void onReceive(Context context, Intent intent) {

			// Check the action code and determine what to do
			String action = intent.getAction();

			// Intent contains information about errors in adding or removing
			// geofences
			if (TextUtils.equals(action, GeofenceUtils.ACTION_GEOFENCE_ERROR)) {

				handleGeofenceError(context, intent);

				// Intent contains information about successful addition or
				// removal of geofences
			} else if (TextUtils.equals(action,
					GeofenceUtils.ACTION_GEOFENCES_ADDED)
					|| TextUtils.equals(action,
							GeofenceUtils.ACTION_GEOFENCES_REMOVED)) {

				handleGeofenceStatus(context, intent);

				// Intent contains information about a geofence transition
			} else if (TextUtils.equals(action,
					GeofenceUtils.ACTION_GEOFENCE_TRANSITION)) {

				handleGeofenceTransition(context, intent);

				// The Intent contained an invalid action
			} else {
				Log.e(GeofenceUtils.APPTAG,
						getString(R.string.invalid_action_detail, action));
				Toast.makeText(context, R.string.invalid_action,
						Toast.LENGTH_LONG).show();
			}
		}

		/**
		 * If you want to display a UI message about adding or removing
		 * geofences, put it here.
		 * 
		 * @param context
		 *            A Context for this component
		 * @param intent
		 *            The received broadcast Intent
		 */
		private void handleGeofenceStatus(Context context, Intent intent) {

		}

		/**
		 * Report geofence transitions to the UI
		 * 
		 * @param context
		 *            A Context for this component
		 * @param intent
		 *            The Intent containing the transition
		 */
		private void handleGeofenceTransition(Context context, Intent intent) {
			/*
			 * If you want to change the UI when a transition occurs, put the
			 * code here. The current design of the app uses a notification to
			 * inform the user that a transition has occurred.
			 */
		}

		/**
		 * Report addition or removal errors to the UI, using a Toast
		 * 
		 * @param intent
		 *            A broadcast Intent sent by ReceiveTransitionsIntentService
		 */
		private void handleGeofenceError(Context context, Intent intent) {
			String msg = intent
					.getStringExtra(GeofenceUtils.EXTRA_GEOFENCE_STATUS);
			Log.e(GeofenceUtils.APPTAG, msg);
			Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Called when the user clicks the "Register geofences" button. Get the
	 * geofence parameters for each geofence and add them to a List. Create the
	 * PendingIntent containing an Intent that Location Services sends to this
	 * app's broadcast receiver when Location Services detects a geofence
	 * transition. Send the List and the PendingIntent to Location Services.
	 */
	public void onRegisterClicked() {

		/*
		 * Record the request as an ADD. If a connection error occurs, the app
		 * can automatically restart the add request if Google Play services can
		 * fix the error
		 */
		mRequestType = GeofenceUtils.REQUEST_TYPE.ADD;

		/*
		 * Check for Google Play services. Do this after setting the request
		 * type. If connecting to Google Play services fails, onActivityResult
		 * is eventually called, and it needs to know what type of request was
		 * in progress.
		 */
		if (!servicesConnected()) {

			return;
		}

		/*
		 * Create a version of geofence 2 that is "flattened" into individual
		 * fields. This allows it to be stored in SharedPreferences.
		 */
		mUIGeofence2 = new SimpleGeofence("2",
		// Get latitude, longitude, and radius from the UI
				Double.valueOf(latLng.latitude),
				Double.valueOf(latLng.longitude), Float.valueOf(radius),
				// Set the expiration time
				GEOFENCE_EXPIRATION_IN_MILLISECONDS,
				// Detect both entry and exit transitions
				Geofence.GEOFENCE_TRANSITION_ENTER
						| Geofence.GEOFENCE_TRANSITION_EXIT);

		// Store this flat version in SharedPreferences
		mPrefs.setGeofence("2", mUIGeofence2);

		/*
		 * Add Geofence objects to a List. toGeofence() creates a Location
		 * Services Geofence object from a flat object
		 */
		mCurrentGeofences.add(mUIGeofence2.toGeofence());

		// Start the request. Fail if there's already a request in progress
		try {
			// Try to add geofences
			mGeofenceRequester.addGeofences(mCurrentGeofences);
		} catch (UnsupportedOperationException e) {
			// Notify user that previous request hasn't finished.
			Toast.makeText(getActivity(),
					R.string.add_geofences_already_requested_error,
					Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Called when the user clicks the "Remove geofence 2" button
	 * 
	 * @param view
	 *            The view that triggered this callback
	 */

	public void onUnregisterGeofence2Clicked() {
		/*
		 * Remove the geofence by creating a List of geofences to remove and
		 * sending it to Location Services. The List contains the id of geofence
		 * 2, which is "2". The removal happens asynchronously; Location
		 * Services calls onRemoveGeofencesByPendingIntentResult() (implemented
		 * in the current Activity) when the removal is done.
		 */

		/*
		 * Record the removal as remove by list. If a connection error occurs,
		 * the app can automatically restart the removal if Google Play services
		 * can fix the error
		 */
		mRemoveType = GeofenceUtils.REMOVE_TYPE.LIST;

		// Create a List of 1 Geofence with the ID "2" and store it in the
		// global list
		mGeofenceIdsToRemove = Collections.singletonList("2");

		/*
		 * Check for Google Play services. Do this after setting the request
		 * type. If connecting to Google Play services fails, onActivityResult
		 * is eventually called, and it needs to know what type of request was
		 * in progress.
		 */
		if (!servicesConnected()) {

			return;
		}

		// Try to remove the geofence
		try {
			if (mGeofenceRemover != null) {
				mGeofenceRemover.removeGeofencesById(mGeofenceIdsToRemove);
				mPrefs.clearGeofence("2");
			}

			// Catch errors with the provided geofence IDs
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (UnsupportedOperationException e) {
			// Notify user that previous request hasn't finished.
			Toast.makeText(getActivity(),
					R.string.remove_geofences_already_requested_error,
					Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Verify that Google Play services is available before making a request.
	 * 
	 * @return true if Google Play services is available, otherwise false
	 */
	private boolean servicesConnected() {

		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getActivity());

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {

			// In debug mode, log the status
			Log.d(GeofenceUtils.APPTAG,
					getString(R.string.play_services_available));

			// Continue
			return true;

			// Google Play services was not available for some reason
		} else {

			// Display an error dialog
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					getActivity(), 0);
			if (dialog != null) {
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				errorFragment.setDialog(dialog);
				errorFragment.show(getChildFragmentManager(),
						GeofenceUtils.APPTAG);
			}
			return false;
		}
	}

	/**
	 * Define a DialogFragment to display the error dialog generated in
	 * showErrorDialog.
	 */
	public static class ErrorDialogFragment extends DialogFragment {

		// Global field to contain the error dialog
		private Dialog mDialog;

		/**
		 * Default constructor. Sets the dialog field to null
		 */
		public ErrorDialogFragment() {
			super();
			mDialog = null;
		}

		/**
		 * Set the dialog to display
		 * 
		 * @param dialog
		 *            An error dialog
		 */
		public void setDialog(Dialog dialog) {
			mDialog = dialog;
		}

		/*
		 * This method must return a Dialog to the DialogFragment.
		 */
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			return mDialog;
		}
	}

	@Override
	public void asyncResponse(int request, String response) {
		UserInfoModel model;
		switch (request) {
		case SERVICE_REQUEST_GET_USER:
			model = JsonParser.getUserInfo(response);
			if (model != null) {
				updateUiElement(model);

			}
			break;
		case SERVICE_REQUEST_GET_UPDATE:
			model = JsonParser.getUserInfo(response);
			if (model != null) {
				Toast.makeText(getActivity(), "Succesfully updated user info",
						Toast.LENGTH_SHORT).show();
				updateUserToPreference(model);
			}
			break;

		default:
			break;
		}

	}

	private void updateUserToPreference(UserInfoModel model) {
		userPrefs.setUserInfo(UserInfoUtil.KEY_USER, model);
		String geoFenceStatus = model.getGeoFenceStatus();
		if (geoFenceStatus != null) {
			if (geoFenceStatus.equalsIgnoreCase("no")
					|| !geoFenceStatus.equalsIgnoreCase("yes")) {
				onUnregisterGeofence2Clicked();
			}
		}
	}
}
