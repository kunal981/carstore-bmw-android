package com.farzain.carstore.fragment.setting;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;
import com.farzain.carstore.R;

public class SettingFragmentBackUp extends Fragment {

	private static final int ZBAR_SCANNER_REQUEST = 0;
	private static final int ZBAR_QR_SCANNER_REQUEST = 1;

	/**
	 * The fragment argument representing the section number for this fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";

	Button buttonScan;
	EditText editVechileNumber;

	ToggleButton toogleServiceOne, toogleServiceGeoCoding;

	// toogleServiceTwo
	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static SettingFragmentBackUp newInstance(int sectionNumber) {
		SettingFragmentBackUp fragment = new SettingFragmentBackUp();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_layout_setting,
				container, false);

		editVechileNumber = (EditText) rootView
				.findViewById(R.id.edit_setting_vechile_info);
		buttonScan = (Button) rootView.findViewById(R.id.btn_scan);

		toogleServiceOne = (ToggleButton) rootView
				.findViewById(R.id.toggleButton1);
		// toogleServiceTwo = (ToggleButton) rootView
		// .findViewById(R.id.toggleButton2);
		toogleServiceGeoCoding = (ToggleButton) rootView
				.findViewById(R.id.toggleButton_geo_fence);

		addToggleClickListener();

		buttonScan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				launchScanner(v);
			}
		});

		return rootView;
	}

	private void addToggleClickListener() {
		toogleServiceOne
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean isChecked) {
						Toast.makeText(getActivity(),
								"Toggle One:" + isChecked, Toast.LENGTH_SHORT)
								.show();
					}
				});
		// toogleServiceTwo
		// .setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//
		// @Override
		// public void onCheckedChanged(CompoundButton arg0,
		// boolean isChecked) {
		// Toast.makeText(getActivity(),
		// "Toggle Two:" + isChecked, Toast.LENGTH_SHORT)
		// .show();
		// }
		// });
		toogleServiceGeoCoding
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean isChecked) {
						Toast.makeText(getActivity(),
								"Toggle Three: " + isChecked,
								Toast.LENGTH_SHORT).show();
					}
				});
	}

	public void launchScanner(View v) {
		if (isCameraAvailable()) {
			Intent intent = new Intent(getActivity(), ZBarScannerActivity.class);
			startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
		} else {
			Toast.makeText(getActivity(), "Rear Facing Camera Unavailable",
					Toast.LENGTH_SHORT).show();
		}
	}

	public boolean isCameraAvailable() {
		PackageManager pm = getActivity().getPackageManager();
		return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case ZBAR_SCANNER_REQUEST:
			if (resultCode == Activity.RESULT_OK) {
				// Toast.makeText(
				// getActivity(),
				// "Scan Result = "
				// + data.getStringExtra(ZBarConstants.SCAN_RESULT),
				// Toast.LENGTH_SHORT).show();
				if (data != null) {
					String vinNumber = data
							.getStringExtra(ZBarConstants.SCAN_RESULT);
					editVechileNumber.setText(vinNumber);
				}

			} else if (resultCode == Activity.RESULT_CANCELED && data != null) {
				String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
				if (!TextUtils.isEmpty(error)) {
					Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT)
							.show();
				}
			}
			break;
		}
	}

}
