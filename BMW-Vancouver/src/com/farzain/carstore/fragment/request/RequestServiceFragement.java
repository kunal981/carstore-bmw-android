package com.farzain.carstore.fragment.request;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.farzain.carstore.R;
import com.farzain.carstore.adapter.OnAsyncRequestComplete;
import com.farzain.carstore.controller.UiHandler;
import com.farzain.carstore.model.CompanyModel;
import com.farzain.carstore.model.UserInfoModel;
import com.farzain.carstore.network.AsyncRequest;
import com.farzain.carstore.parser.JsonParser;
import com.farzain.carstore.pref.UserStore;
import com.farzain.carstore.util.AppConstant;
import com.farzain.carstore.util.Log;
import com.farzain.carstore.util.AppConstant.UserInfoUtil;
import com.farzain.carstore.ws.WSConstant;
import com.farzain.carstore.ws.WSConstant.PARAM;
import com.farzain.carstore.ws.WSConstant.VALUES;
import com.squareup.picasso.Picasso;

public class RequestServiceFragement extends Fragment implements
		OnAsyncRequestComplete {

	/**
	 * The fragment argument representing the section number for this fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";

	public String companyNumber;
	public String companyEmail;
	public String roadAssitanceNumber;

	Button buttonCall, buttonEmail, buttonRoadAssist;
	ImageView imageBanner;
	TextView textMsg;

	UiHandler uHandler;
	CompanyModel cmpModel;
	UserStore userPrefs;

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static RequestServiceFragement newInstance(int sectionNumber) {
		RequestServiceFragement fragment = new RequestServiceFragement();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uHandler = UiHandler.getInstance();
		cmpModel = uHandler.getCompanyInfo();
		userPrefs = new UserStore(getActivity());
		if (cmpModel == null) {
			cmpModel = new CompanyModel();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.fragment_layout_request_service, container, false);
		imageBanner = (ImageView) rootView.findViewById(R.id.img_home_banner);
		buttonCall = (Button) rootView.findViewById(R.id.button_call);
		buttonRoadAssist = (Button) rootView
				.findViewById(R.id.button_road_assist);

		buttonEmail = (Button) rootView.findViewById(R.id.button_email);
		textMsg = (TextView) rootView.findViewById(R.id.text_home_tile_msg);

		imageBanner.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				callService(AppConstant.PHONE_CAR_STORE);
			}
		});
		executeRequestServiceNetworkTask();
		return rootView;
	}

	private void executeRequestServiceNetworkTask() {
		String url = null;
		url = WSConstant.API_GET + PARAM.ACTION + "="
				+ VALUES.ACTION_REQUEST_SERVICE + "&" + PARAM.COMPANY_APP_ID
				+ "=" + VALUES.COMPANY_APP_ID;
		AsyncRequest getHomeInfoTask = new AsyncRequest(getActivity(), this,
				"GET");
		getHomeInfoTask.execute(url);
	}

	private void updatUiItem(CompanyModel model) {
		Picasso.with(getActivity()).load(model.getImageBanner())
				.resize(400, 200).centerCrop().into(imageBanner);
		textMsg.setText(model.getRequestMsg());

		companyNumber = model.getCompanyCall();
		companyEmail = model.getEmail();
		roadAssitanceNumber = model.getRsAssistNumber();
		uHandler.setCompanyInfo(model);
		addClickListenerButton();
	}

	private void addClickListenerButton() {

		buttonCall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Toast.makeText(getActivity(), "Calling", Toast.LENGTH_SHORT)
						.show();
				// callService(AppConstant.PHONE_CAR_STORE);
				callService(companyNumber);

			}
		});
		buttonEmail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Toast.makeText(getActivity(), "Email", Toast.LENGTH_SHORT)
						.show();
				emailService();

			}
		});
		buttonRoadAssist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// callService(AppConstant.PHONE_ROAD_ASSIST);
				callService(roadAssitanceNumber);
			}
		});

	}

	protected void emailService() {

		UserInfoModel user = userPrefs.getUserInfo(UserInfoUtil.KEY_USER);
		if (!user.getVehicleNum().equals("")) {

			String emailTemplete = String.valueOf("VIN: "
					+ user.getVehicleNum() + "+\nCustomer Name: "
					+ user.getName() + "\nCustomer phone number(HOME): "
					+ user.getPhoneHome() + "\nCustomer phone number(MOBILE): "
					+ user.getPhoneMobile()
					+ "\nCustomer phone number(BUSINESS): "
					+ user.getPhoneBusiness() + "\nDrop off required:  "
					+ user.getServiceOptionStatus()
					+ "\nRepair completion text required: "
					+ user.getServiceOptionRepair());

			Intent email = new Intent(Intent.ACTION_SEND);
			// email.putExtra(Intent.EXTRA_EMAIL,
			// new String[] { getString(R.string.text_email_to) });
			email.putExtra(Intent.EXTRA_EMAIL, new String[] { companyEmail });
			// email.putExtra(Intent.EXTRA_CC, new String[]{ to});
			email.putExtra(Intent.EXTRA_BCC,
					new String[] { getString(R.string.text_email_bcc) });
			email.putExtra(Intent.EXTRA_SUBJECT,
					getString(R.string.text_email_sub));

			email.putExtra(Intent.EXTRA_TEXT, emailTemplete);

			// need this to prompts email client only
			email.setType("message/rfc822");

			startActivity(Intent.createChooser(email,
					"Choose an Email client :"));

		} else {
			Log.showAlertDialog(getActivity(), "User Detail",
					"Please setup your user detail on setting page first",
					false);
		}

	}

	public void callService(String number) {

		String uri2 = "tel:" + number;
		Intent intent2 = new Intent(Intent.ACTION_CALL);
		intent2.setData(Uri.parse(uri2));
		startActivity(intent2);
	}

	@Override
	public void asyncResponse(int request, String response) {
		CompanyModel model = JsonParser.getCompanyRequest(response);
		if (model != null) {
			cmpModel.setRequestMsg(model.getRequestMsg());
			updatUiItem(cmpModel);
		} else {
			Toast.makeText(getActivity(), "Unable to get information",
					Toast.LENGTH_SHORT).show();
		}

	}

}
