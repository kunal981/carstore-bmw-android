package com.farzain.carstore.pref;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.farzain.carstore.MainActivity;
import com.farzain.carstore.model.UserInfoModel;
import com.farzain.carstore.util.AppConstant.UserInfoUtil;

/**
 * Storage for User values, implemented in SharedPreferences. For a production
 * app, use a content provider that's synced to the web or loads geofence data
 * based on current location.
 */
public class UserStore {

	// The SharedPreferences object in which geofences are stored
	private final SharedPreferences mPrefs;

	// The name of the resulting SharedPreferences
	private static final String SHARED_PREFERENCE_NAME = UserStore.class
			.getSimpleName();

	// Create the SharedPreferences storage with private access only
	public UserStore(Context context) {
		mPrefs = context.getSharedPreferences(SHARED_PREFERENCE_NAME,
				Context.MODE_PRIVATE);
	}

	public UserInfoModel getUserInfo(String id) {

		String name = mPrefs.getString(
				getUserFieldKey(id, UserInfoUtil.KEY_NAME), "");
		String vehicleNum = mPrefs.getString(
				getUserFieldKey(id, UserInfoUtil.KEY_VIN), "");
		String phoneMobile = mPrefs.getString(
				getUserFieldKey(id, UserInfoUtil.KEY_PHONE_MOBILE), "");
		String phoneHome = mPrefs.getString(
				getUserFieldKey(id, UserInfoUtil.KEY_PHONE_HOME), "");
		String phoneBusiness = mPrefs.getString(
				getUserFieldKey(id, UserInfoUtil.KEY_PHONE_BUSINESS), "");
		String dropOffStatus = mPrefs.getString(
				getUserFieldKey(id, UserInfoUtil.KEY_PHONE_DROP_OFF), "");
		String repairStatus = mPrefs.getString(
				getUserFieldKey(id, UserInfoUtil.KEY_PHONE_REPAIR_COM), "");

		UserInfoModel user = new UserInfoModel();
		user.setName(name);
		user.setVehicleNum(vehicleNum);
		user.setPhoneMobile(phoneMobile);
		user.setPhoneBusiness(phoneBusiness);
		user.setPhoneHome(phoneHome);
		user.setServiceOptionStatus(dropOffStatus);
		user.setServiceOptionRepair(repairStatus);

		return user;

	}

	public void setUserInfo(String id, UserInfoModel userInfoModel) {

		/*
		 * Get a SharedPreferences editor instance. Among other things,
		 * SharedPreferences ensures that updates are atomic and non-concurrent
		 */
		Editor editor = mPrefs.edit();

		// Write the Geofence values to SharedPreferences
		editor.putString(
				getUserFieldKey(id, UserInfoUtil.KEY_NAME),
				String.valueOf(userInfoModel.getFirstName() + " "
						+ userInfoModel.getLastName()));
		editor.putString(getUserFieldKey(id, UserInfoUtil.KEY_VIN),
				String.valueOf(userInfoModel.getVehicleNum()));

		if (userInfoModel.getPhoneMobile() != null
				&& !userInfoModel.getPhoneMobile().equals("")) {
			editor.putString(
					getUserFieldKey(id, UserInfoUtil.KEY_PHONE_MOBILE),
					userInfoModel.getPhoneMobile());
		}
		if (userInfoModel.getPhoneBusiness() != null
				&& !userInfoModel.getPhoneBusiness().equals("")) {
			editor.putString(
					getUserFieldKey(id, UserInfoUtil.KEY_PHONE_BUSINESS),
					userInfoModel.getPhoneBusiness());
		}
		if (userInfoModel.getPhoneHome() != null
				&& !userInfoModel.getPhoneHome().equals("")) {
			editor.putString(getUserFieldKey(id, UserInfoUtil.KEY_PHONE_HOME),
					userInfoModel.getPhoneHome());
		}

		if (userInfoModel.getServiceOptionStatus() != null
				&& !userInfoModel.getServiceOptionStatus().equals("")) {
			editor.putString(
					getUserFieldKey(id, UserInfoUtil.KEY_PHONE_DROP_OFF),
					userInfoModel.getServiceOptionStatus());
		}
		if (userInfoModel.getServiceOptionRepair() != null
				&& !userInfoModel.getServiceOptionRepair().equals("")) {
			editor.putString(
					getUserFieldKey(id, UserInfoUtil.KEY_PHONE_REPAIR_COM),
					userInfoModel.getServiceOptionRepair());
		}
		editor.commit();

	}

	public void clearGeofence(String id) {

		// Remove a flattened geofence object from storage by removing all of
		// its keys
		Editor editor = mPrefs.edit();
		editor.remove(getUserFieldKey(id, UserInfoUtil.KEY_NAME));
		editor.remove(getUserFieldKey(id, UserInfoUtil.KEY_VIN));
		editor.remove(getUserFieldKey(id, UserInfoUtil.KEY_PHONE_BUSINESS));
		editor.remove(getUserFieldKey(id, UserInfoUtil.KEY_PHONE_HOME));
		editor.remove(getUserFieldKey(id, UserInfoUtil.KEY_PHONE_MOBILE));
		editor.remove(getUserFieldKey(id, UserInfoUtil.KEY_PHONE_DROP_OFF));
		editor.remove(getUserFieldKey(id, UserInfoUtil.KEY_PHONE_REPAIR_COM));

		editor.commit();

	}

	/**
	 * Given a Geofence object's ID and the name of a field (for example,
	 * GeofenceUtils.KEY_LATITUDE), return the key name of the object's values
	 * in SharedPreferences.
	 * 
	 * @param id
	 *            The ID of a Geofence object
	 * @param fieldName
	 *            The field represented by the key
	 * @return The full key name of a value in SharedPreferences
	 */
	private String getUserFieldKey(String id, String fieldName) {

		return UserInfoUtil.KEY_PREFIX + id + "_" + fieldName;
	}

}
