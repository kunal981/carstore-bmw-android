package com.dealernx.bavaria.bmw.util;

public class AppConstant {

	public static final String PHONE_CAR_STORE = "7804840000";
	public static final String PHONE_ROAD_ASSIST = "18002678269";

	public static final String EMAIl_CAR_STORE = "info@bavariabmw.ca";

	/**
	 * not used currently
	 * 
	 * @author lenovo
	 *
	 */
	public class UserInfoUtil {

		// The prefix for flattened geofence keys

		public static final String KEY_USER = "com.dealernx.bavaria.bmw.USER";
		public static final String KEY_PREFIX = "com.dealernx.bavaria.bmw.KEY";
		public static final String KEY_NAME = "com.dealernx.bavaria.bmw.KEY_NAME";
		public static final String KEY_VIN = "com.dealernx.bavaria.bmw.KEY_VIND";
		public static final String KEY_PHONE_HOME = "com.dealernx.bavaria.bmw.KEY_PHONE_HOME";
		public static final String KEY_PHONE_MOBILE = "com.dealernx.bavaria.bmw.KEY_PHONE_MOBILE";
		public static final String KEY_PHONE_BUSINESS = "com.dealernx.bavaria.bmw.KEY_PHONE_BUSINESS";
		public static final String KEY_PHONE_DROP_OFF = "com.dealernx.bavaria.bmw.KEY_PHONE_DROP_OFF";
		public static final String KEY_PHONE_REPAIR_COM = "com.dealernx.bavaria.bmw.KEY_PHONE_REPAIR_COM";

	}

}
