package com.dealernx.bavaria.bmw.fragment.instrument;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.dealernx.bavaria.bmw.R;
import com.dealernx.bavaria.bmw.adapter.CustomGridViewAdapter;
import com.dealernx.bavaria.bmw.adapter.Item;
import com.dealernx.bavaria.bmw.adapter.OnAsyncRequestComplete;
import com.dealernx.bavaria.bmw.controller.UiHandler;
import com.dealernx.bavaria.bmw.model.CompanyModel;
import com.dealernx.bavaria.bmw.network.AsyncRequest;
import com.dealernx.bavaria.bmw.parser.JsonParser;
import com.dealernx.bavaria.bmw.popup.PopUpActivity;
import com.dealernx.bavaria.bmw.pref.UserStore;
import com.dealernx.bavaria.bmw.ws.WSConstant;
import com.dealernx.bavaria.bmw.ws.WSConstant.PARAM;
import com.dealernx.bavaria.bmw.ws.WSConstant.VALUES;

public class InstrumentClusterFragment extends Fragment implements
		OnAsyncRequestComplete {

	/**
	 * The fragment argument representing the section number for this fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";

	Button buttonCall, buttonEmail;
	ImageView imageBanner;
	WebView myWebView;

	UiHandler uHandler;
	CompanyModel cmpModel;
	GridView gridView;
	List<Item> listData;
	CustomGridViewAdapter customGridAdapter;

	public String companyNumber;
	public String companyEmail;
	public String roadAssitanceNumber;
	private UserStore userPrefs;

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static InstrumentClusterFragment newInstance(int sectionNumber) {
		InstrumentClusterFragment fragment = new InstrumentClusterFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uHandler = UiHandler.getInstance();
		userPrefs = new UserStore(getActivity());
		cmpModel = uHandler.getCompanyInfo();
		listData = new ArrayList<Item>();
		if (cmpModel == null) {
			cmpModel = new CompanyModel();
		}
	}

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.fragment_layout_instrument_cluster, container, false);

		// set grid view item
		gridView = (GridView) rootView.findViewById(R.id.gridView1);
		customGridAdapter = new CustomGridViewAdapter(getActivity(),
				R.layout.row_grid, listData);
		gridView.setAdapter(customGridAdapter);
		gridView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				Item item = listData.get(position);
				Intent m_Intent = new Intent(getActivity(), PopUpActivity.class);
				m_Intent.putExtra("IMAGEPATH", item.getImageUrl());
				m_Intent.putExtra("CAUSE", item.getCause());
				m_Intent.putExtra("ACTION", item.getAction());
				startActivity(m_Intent);
			}

		});

		executeInstrumentNetworkTask();

		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	private void executeInstrumentNetworkTask() {
		String url = null;
		String companyId = null;
		try {
			companyId = URLEncoder.encode(VALUES.COMPANY_APP_ID, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		url = WSConstant.API_GET_INSTRUMENT + PARAM.PAGE + "="
				+ VALUES.PAGE_VALUE + "&" + PARAM.ACTION + "="
				+ VALUES.ACTION_INSTRUMENT_CLUSTER_DATA + "&"
				+ PARAM.COMPANY_APP_ID + "=" + "BMW_716"; // for the timebeing
															// set this to
															// bmw_716 as both
															// store has same
															// instrument

		AsyncRequest getHomeInfoTask = new AsyncRequest(getActivity(), this,
				"GET");
		getHomeInfoTask.execute(url);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setRetainInstance(true);
	}

	public void callService(String number) {

		String uri2 = "tel:" + number;
		Intent intent2 = new Intent(Intent.ACTION_CALL);
		intent2.setData(Uri.parse(uri2));
		startActivity(intent2);
	}

	@Override
	public void asyncResponse(int request, String response) {

		listData = JsonParser.getListInstrument(response);

		if (listData != null && listData.size() != 0) {
			updateGirdItemns();

		}

	}

	private void updateGirdItemns() {
		customGridAdapter.update(listData);
	}
}
