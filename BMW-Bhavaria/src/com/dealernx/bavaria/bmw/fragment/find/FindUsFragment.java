package com.dealernx.bavaria.bmw.fragment.find;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dealernx.bavaria.bmw.R;
import com.dealernx.bavaria.bmw.controller.UiHandler;
import com.dealernx.bavaria.bmw.model.CompanyModel;
import com.dealernx.bavaria.bmw.util.PathJSONParser;
import com.dealernx.bavaria.bmw.ws.WSAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class FindUsFragment extends Fragment implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {
	/**
	 * The fragment argument representing the section number for this fragment.
	 */

	public static final String TAG = FindUsFragment.class.getSimpleName();

	View rootView;
	private static final String ARG_SECTION_NUMBER = "section_number";

	public String address = "1740 W 5th Ave.Vancouver B.C. V6J 1P2";

	private GoogleMap mMap;
	private MapView mMapView;
	private Polyline line;

	LatLng destinationLatLng;
	LatLng sourceLatLng;

	// location finder
	private boolean isLocationManager = false;
	// A request to connect to Location Services
	private LocationRequest mLocationRequest;
	// Stores the current instantiation of the location client in this object
	private LocationClient mLocationClient;

	Location currentLocation;
	double latitude;
	double longitude;

	UiHandler uHandler;
	CompanyModel cmpModel;

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static FindUsFragment newInstance(int sectionNumber) {
		FindUsFragment fragment = new FindUsFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public FindUsFragment() {
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		uHandler = UiHandler.getInstance();
		cmpModel = uHandler.getCompanyInfo();
		if (cmpModel != null) {
			double lat = Double.parseDouble(cmpModel.getLatitude());
			double lng = Double.parseDouble(cmpModel.getLongitude());
			destinationLatLng = new LatLng(lat, lng);
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_layout_map,
					container, false);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}

		MapsInitializer.initialize(getActivity());
		mMapView = (MapView) rootView.findViewById(R.id.map);
		mMapView.onCreate(savedInstanceState);

		setUpMapAndUserCurrentLocation();

		return rootView;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.map, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// handle item selection
		switch (item.getItemId()) {
		case R.id.action_route:
			// do s.th.
			com.dealernx.bavaria.bmw.util.Log.LOG(getActivity(), "get direction");
			if (sourceLatLng != null && destinationLatLng != null) {
				addPolyLine();
			} else {
				com.dealernx.bavaria.bmw.util.Log.LOG(getActivity(),
						"unable to get direction");
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void setUpMapAndUserCurrentLocation() {
		if (mMap == null) {
			LatLng clatLng;
			mMap = mMapView.getMap();
			mMap.getUiSettings().setMyLocationButtonEnabled(false);
			mMap.setMyLocationEnabled(true);
			// Updates the location and zoom of the MapView
			Location loc = mMap.getMyLocation();
			if (loc != null) {
				clatLng = new LatLng(loc.getLatitude(), loc.getLongitude());
			} else {
				clatLng = new LatLng(49.2756d, -123.12717d);
				// clatLng = new LatLng(49.2756d, -123.12717d);
			}
			mMap.moveCamera(CameraUpdateFactory.newLatLng(clatLng));
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
					clatLng, 12);
			mMap.animateCamera(cameraUpdate);
			updateMapElement(cmpModel);

		}

	}

	private void updateMapElement(CompanyModel model) {
		LatLng clatLng = null;
		if (model.getLatitude() != null && !model.getLatitude().equals("")
				&& model.getLongitude() != null
				&& !model.getLongitude().equals("")) {
			double lat = Double.parseDouble(model.getLatitude());
			double lng = Double.parseDouble(model.getLongitude());
			clatLng = new LatLng(lat, lng);
		}

		if (mMap != null)
			if (clatLng != null) {
				mMap.addMarker(new MarkerOptions()
						.position(clatLng)
						.title(model.getComapanyName())
						.snippet(
								String.valueOf(model.getAddress()
										+ "\n Contact: "
										+ model.getCompanyCall()))
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.ic_map)));
				mMap.moveCamera(CameraUpdateFactory.newLatLng(clatLng));
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
						clatLng, 15);
				mMap.animateCamera(cameraUpdate);

			}

	}

	// Google play services for detecting location demo

	private void locationInitilizer() {

		if (servicesConnected()) {
			LocationManager locationManager = (LocationManager) getActivity()
					.getSystemService(Context.LOCATION_SERVICE);
			if (locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
					|| locationManager
							.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				mLocationRequest = LocationRequest.create();
				mLocationRequest
						.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
				mLocationRequest.setInterval(1000);
				mLocationRequest.setFastestInterval(500);
				mLocationClient = new LocationClient(getActivity(), this, this);
				mLocationClient.connect();
				isLocationManager = true;
			} else {
				showSettingsAlert();
			}

		}

	}

	@Override
	public void onResume() {
		super.onResume();
		mMapView.onResume();
		locationInitilizer();
	}

	@Override
	public void onPause() {
		mMapView.onPause();
		super.onPause();
		mMap = null;
	}

	@Override
	public void onDestroy() {
		mMapView.onDestroy();
		super.onDestroy();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mMapView.onLowMemory();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mMapView.onSaveInstanceState(outState);
	}

	private class ReadTask extends AsyncTask<String, Void, String> {

		ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Getting directions");
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... url) {
			String data = "";
			try {
				data = WSAdapter.getJSONObject(url[0]);
			} catch (Exception e) {
				Log.d("Background Task", e.toString());
			}
			return data;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (pDialog.isShowing())
				pDialog.dismiss();
			new ParserTask().execute(result);
		}
	}

	private class GetAddressTask extends AsyncTask<String, Void, JSONObject> {

		/*
		 * When the task finishes, onPostExecute() displays the address. 
		 */
		@Override
		protected void onPostExecute(JSONObject jsonObject) {
			// Display the current address in the UI

			boolean status = getLatLong(jsonObject);
			// if (status) {
			// addPolyLine();
			// }
			Log.v(TAG, "Location parse from address");

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			JSONObject jsonObjectLatLng = null;
			try {
				jsonObjectLatLng = getJsonForLatLong(params[0]);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return jsonObjectLatLng;

		}
	}

	private class ParserTask extends
			AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

		@Override
		protected List<List<HashMap<String, String>>> doInBackground(
				String... jsonData) {

			JSONObject jObject;
			List<List<HashMap<String, String>>> routes = null;

			try {
				jObject = new JSONObject(jsonData[0]);
				PathJSONParser parser = new PathJSONParser();
				routes = parser.parse(jObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return routes;
		}

		@Override
		protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
			ArrayList<LatLng> points = null;
			PolylineOptions polyLineOptions = null;

			// traversing through routes
			for (int i = 0; i < routes.size(); i++) {
				points = new ArrayList<LatLng>();
				polyLineOptions = new PolylineOptions();
				List<HashMap<String, String>> path = routes.get(i);

				for (int j = 0; j < path.size(); j++) {
					HashMap<String, String> point = path.get(j);

					double lat = Double.parseDouble(point.get("lat"));
					double lng = Double.parseDouble(point.get("lng"));
					LatLng position = new LatLng(lat, lng);

					points.add(position);
				}

				polyLineOptions.addAll(points);
				polyLineOptions.width(5);
				polyLineOptions.color(Color.BLUE);
			}

			if (polyLineOptions != null) {
				mMap.addPolyline(polyLineOptions);
				addMarkers();
			} else {
				com.dealernx.bavaria.bmw.util.Log.showAlertDialog(getActivity(),
						"No Route", "Unable to find route", false);
			}

		}
	}

	public JSONObject getJsonForLatLong(String address) throws IOException {
		StringBuilder stringBuilder = new StringBuilder();
		InputStream stream = null;
		try {

			address = address.replaceAll(" ", "%20");

			HttpPost httppost = new HttpPost(
					"http://maps.google.com/maps/api/geocode/json?address="
							+ address + "&sensor=false");
			HttpClient client = new DefaultHttpClient();
			HttpResponse response;
			stringBuilder = new StringBuilder();

			response = client.execute(httppost);
			HttpEntity entity = response.getEntity();
			stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		} catch (ClientProtocolException e) {
		} catch (IOException e) {
		} finally {
			stream.close();
		}

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject(stringBuilder.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonObject;
	}

	private String getMapsApiDirectionsUrl() {
		String waypoints = "origin=" + sourceLatLng.latitude + ","
				+ sourceLatLng.longitude + "&destination="
				+ destinationLatLng.latitude + ","
				+ destinationLatLng.longitude + "";

		String sensor = "sensor=false";
		String params = waypoints + "&" + sensor;
		String output = "json";
		String url = "https://maps.googleapis.com/maps/api/directions/"
				+ output + "?" + params;
		return url;

		// https://maps.googleapis.com/maps/api/directions/json?origin=49.267143,-123.143522&destination=49.261013,-123.153801&sensor=false&mode=driving&alternatives=true"

	}

	public void addPolyLine() {
		String url = getMapsApiDirectionsUrl();
		ReadTask downloadTask = new ReadTask();
		downloadTask.execute(url);

	}

	private void addMarkers() {

		if (mMap != null)
			mMap.addMarker(new MarkerOptions()
					.position(sourceLatLng)
					.title("Current Location")
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.ic_map)));
		// mMap.addMarker(new MarkerOptions().position(destinationLatLng)
		// .title(cmpModel.getComapanyName())
		// .snippet(cmpModel.getAddress())
		// .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map)));

		mMap.moveCamera(CameraUpdateFactory.newLatLng(sourceLatLng));
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
				sourceLatLng, 16);
		mMap.animateCamera(cameraUpdate);

	}

	public boolean getLatLong(JSONObject jsonObject) {

		try {

			double latitude = ((JSONArray) jsonObject.get("results"))
					.getJSONObject(0).getJSONObject("geometry")
					.getJSONObject("location").getDouble("lat");
			double longitute = ((JSONArray) jsonObject.get("results"))
					.getJSONObject(0).getJSONObject("geometry")
					.getJSONObject("location").getDouble("lng");

			destinationLatLng = new LatLng(latitude, longitute);
			Toast.makeText(getActivity(),
					"dLatitude: " + latitude + "\ndLongitude:" + longitute,
					Toast.LENGTH_SHORT).show();
			Log.e(TAG, "dLatitude: " + latitude + "\ndLongitude:" + longitute);

		} catch (JSONException e) {
			return false;

		}

		return true;
	}

	public Location getLocation() {
		Location currentLocation = null;
		// If Google Play Services is available
		// Get the current location
		if (mLocationClient.isConnected()) {
			currentLocation = mLocationClient.getLastLocation();
		} else {
			mLocationClient.connect();
		}

		// Display the current location in the UI
		// mLatLng.setText(LocationUtils.getLatLng(this, currentLocation));

		return currentLocation;
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		Log.e("", "Play Service not Available ");
	}

	@Override
	public void onConnected(Bundle arg0) {

		currentLocation = getLocation();
		if (currentLocation != null) {
			// setupUi();
			Toast.makeText(
					getActivity(),
					"Current Latitude: " + currentLocation.getLatitude()
							+ "\nCurrent Longitude: "
							+ currentLocation.getLongitude(),
					Toast.LENGTH_SHORT).show();

			latitude = currentLocation.getLatitude();
			longitude = currentLocation.getLongitude();
			Log.e("Location", "Lat: " + latitude + " Longitude:" + longitude);

			// for current location
			sourceLatLng = new LatLng(currentLocation.getLatitude(),
					currentLocation.getLongitude());

			// sourceLatLng = new LatLng(51.04882, -114.06825);
			// sourceLatLng = new LatLng(49.269262, -123.140812);

			if (destinationLatLng == null) {
				new GetAddressTask().execute(address);
			}
		}

	}

	@Override
	public void onDisconnected() {
	}

	/**
	 * Verify that Google Play services is available before making a request.
	 * 
	 * @return true if Google Play services is available, otherwise false
	 */
	private boolean servicesConnected() {

		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getActivity());

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status

			// Continue
			return true;
			// Google Play services was not available for some reason
		} else {
			// Display an error dialog
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					getActivity(), 0);
			if (dialog != null) {
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				errorFragment.setDialog(dialog);
				errorFragment.show(getChildFragmentManager(), TAG);
			}
			return false;
		}
	}

	/**
	 * Define a DialogFragment to display the error dialog generated in
	 * showErrorDialog.
	 */
	public static class ErrorDialogFragment extends DialogFragment {

		// Global field to contain the error dialog
		private Dialog mDialog;

		/**
		 * Default constructor. Sets the dialog field to null
		 */
		public ErrorDialogFragment() {
			super();
			mDialog = null;
		}

		/**
		 * Set the dialog to display
		 * 
		 * @param dialog
		 *            An error dialog
		 */
		public void setDialog(Dialog dialog) {
			mDialog = dialog;
		}

		/*
		 * This method must return a Dialog to the DialogFragment.
		 */
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			return mDialog;
		}
	}

	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

		// Setting Dialog Title
		alertDialog.setTitle("GPS is settings");

		// Setting Dialog Message
		alertDialog
				.setMessage("GPS is not enabled. Do you want to go to settings menu?");

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(intent);
					}
				});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}
}
