package com.dealernx.bavaria.bmw.fragment.social;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dealernx.bavaria.bmw.R;
import com.dealernx.bavaria.bmw.controller.UiHandler;
import com.dealernx.bavaria.bmw.model.CompanyModel;

public class SocialWebViewFragment extends Fragment {

	private static final String WEB_LINK = "url";

	WebView myWebView;
	String webUrlString;

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static SocialWebViewFragment newInstance(String webUrl) {
		SocialWebViewFragment fragment = new SocialWebViewFragment();
		Bundle args = new Bundle();
		args.putString(WEB_LINK, webUrl);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		webUrlString = getArguments().getString(WEB_LINK);
	}

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_layout_special,
				container, false);
		myWebView = (WebView) rootView.findViewById(R.id.webview);
		myWebView.getSettings().setJavaScriptEnabled(true);

		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();
		setupWebView();
	}

	private class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}

	private void setupWebView() {
		myWebView.getSettings().setBuiltInZoomControls(true);
		myWebView.getSettings().setLoadWithOverviewMode(true);
		myWebView.getSettings().setUseWideViewPort(true);
		myWebView.setWebViewClient(new MyWebViewClient());
		myWebView.loadUrl(webUrlString);
	}

	@Override
	public void onPause() {
		super.onPause();
		myWebView.getSettings().setBuiltInZoomControls(false);
		myWebView.getSettings().setLoadWithOverviewMode(false);
		myWebView.getSettings().setUseWideViewPort(false);
	}

}
