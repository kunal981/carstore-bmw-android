package com.dealernx.bavaria.bmw.fragment.social;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dealernx.bavaria.bmw.MainActivity;
import com.dealernx.bavaria.bmw.R;
import com.dealernx.bavaria.bmw.adapter.OnAsyncRequestComplete;
import com.dealernx.bavaria.bmw.model.SocialLink;
import com.dealernx.bavaria.bmw.network.AsyncRequest;
import com.dealernx.bavaria.bmw.parser.JsonParser;
import com.dealernx.bavaria.bmw.ws.WSConstant;
import com.dealernx.bavaria.bmw.ws.WSConstant.PARAM;
import com.dealernx.bavaria.bmw.ws.WSConstant.VALUES;
import com.squareup.picasso.Picasso;

public class SocialLinksFragment extends Fragment implements
		OnAsyncRequestComplete {

	MainActivity activity;

	// Define the listener of the interface type
	// listener is the activity itself
	private OnItemSelectedListener listener;

	// Define the events that the fragment will use to communicate
	public interface OnItemSelectedListener {
		public void onItemSelected(String link);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof OnItemSelectedListener) {
			listener = (OnItemSelectedListener) activity;
		} else {
			throw new ClassCastException(activity.toString()
					+ " must implement MyListFragment.OnItemSelectedListener");
		}
	}

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static SocialLinksFragment newInstance(int sectionNumber) {
		SocialLinksFragment fragment = new SocialLinksFragment();
		return fragment;
	}

	private ListView listview;
	private TextView textNoInformation;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_layout_social,
				container, false);

		activity = (MainActivity) this.getActivity();
		listview = (ListView) rootView.findViewById(R.id.listview_social);
		textNoInformation = (TextView) rootView.findViewById(R.id.textNoSocial);
		executeSocialNetworkTask();
		return rootView;
	}

	private void executeSocialNetworkTask() {
		String url = null;
		String companyId = null;
		try {
			companyId = URLEncoder.encode(VALUES.COMPANY_APP_ID, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		url = WSConstant.API_GET_INSTRUMENT + PARAM.PAGE + "="
				+ VALUES.PAGE_VALUE + "&" + PARAM.ACTION + "="
				+ VALUES.ACTION_SOCIAL_ICON + "&" + PARAM.COMPANY_APP_ID + "="
				+ companyId;
		AsyncRequest getHomeInfoTask = new AsyncRequest(getActivity(), this,
				"GET");
		getHomeInfoTask.execute(url);
	}

	class ListAdapter1 extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;
		private List<SocialLink> listItem;

		public ListAdapter1(Activity activity, List<SocialLink> listItem) {
			this.activity = activity;
			this.listItem = listItem;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listItem.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return listItem.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			final ViewHolder holder;
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.social_list_item, null);
				holder.imageView = (ImageView) convertView
						.findViewById(R.id.image_id);
				holder.textView = (TextView) convertView
						.findViewById(R.id.title);
				holder.layout = (RelativeLayout) convertView
						.findViewById(R.id.id_layout_rel);
				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			SocialLink socialLink = listItem.get(position);
			holder.textView.setText(socialLink.getName());
			final String url = socialLink.getUrl();
			Picasso.with(getActivity()).load(socialLink.getIcon())
					.into(holder.imageView);

			holder.layout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					// FragmentManager fragmentManager =
					// getChildFragmentManager();
					// fragmentManager
					// .beginTransaction()
					// .replace(R.id.container,
					// SocialWebViewFragment.newInstance(url))
					// .commit();

					listener.onItemSelected(url);

				}
			});

			return convertView;
		}
	}

	class ViewHolder {

		ImageView imageView;
		TextView textView;
		RelativeLayout layout;

	}

	@Override
	public void asyncResponse(int request, String response) {
		List<SocialLink> listItem = JsonParser.getSocialLinkItmes(response);
		updateUiElement(listItem);
	}

	private void updateUiElement(List<SocialLink> listItem) {

		if (listItem != null && listItem.size() != 0) {

			listview.setAdapter(new ListAdapter1(getActivity(), listItem));

		} else {
			listview.setVisibility(View.GONE);
			textNoInformation.setVisibility(View.VISIBLE);
		}

	}
}
