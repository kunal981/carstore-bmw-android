package com.dealernx.bavaria.bmw.fragment.home;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.dealernx.bavaria.bmw.R;
import com.dealernx.bavaria.bmw.adapter.OnAsyncRequestComplete;
import com.dealernx.bavaria.bmw.controller.UiHandler;
import com.dealernx.bavaria.bmw.model.CompanyModel;
import com.dealernx.bavaria.bmw.model.UserInfoModel;
import com.dealernx.bavaria.bmw.network.AsyncRequest;
import com.dealernx.bavaria.bmw.parser.JsonParser;
import com.dealernx.bavaria.bmw.pref.UserStore;
import com.dealernx.bavaria.bmw.util.AppConstant.UserInfoUtil;
import com.dealernx.bavaria.bmw.ws.WSConstant;
import com.dealernx.bavaria.bmw.ws.WSConstant.PARAM;
import com.dealernx.bavaria.bmw.ws.WSConstant.VALUES;
import com.squareup.picasso.Picasso;

public class HomeFragment extends Fragment implements OnAsyncRequestComplete {

	/**
	 * The fragment argument representing the section number for this fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";

	public String companyNumber;
	public String companyEmail;
	public String roadAssitanceNumber;

	View rootView;
	Button buttonCall, buttonEmail, buttonRoadAssist;
	ImageView imageBanner, btnPlay;

	private VideoView videoView;

	String videoUrl;
	private int position = 0;

	private UserStore userPrefs;

	UiHandler uHandler;

	OnAsyncRequestComplete sender;

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static HomeFragment newInstance(int sectionNumber) {
		HomeFragment fragment = new HomeFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uHandler = UiHandler.getInstance();
		userPrefs = new UserStore(getActivity());

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_layout_home,
					container, false);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}

		imageBanner = (ImageView) rootView.findViewById(R.id.img_home_banner);
		buttonCall = (Button) rootView.findViewById(R.id.button_call);
		buttonRoadAssist = (Button) rootView
				.findViewById(R.id.button_road_assist);

		buttonEmail = (Button) rootView.findViewById(R.id.button_email);
		btnPlay = (ImageView) rootView.findViewById(R.id.btn_play);

		// initialize the VideoView
		videoView = (VideoView) rootView.findViewById(R.id.video_view);

		executeHomeNetworkTask();

		return rootView;
	}

	private void executeHomeNetworkTask() {
		String url = null;
		String companyId = null;
		try {
			companyId = URLEncoder.encode(VALUES.COMPANY_APP_ID, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		url = WSConstant.API_GET + PARAM.ACTION + "=" + VALUES.ACTION_HOME
				+ "&" + PARAM.COMPANY_APP_ID + "=" + companyId;
		sender = (OnAsyncRequestComplete) this;
		AsyncRequest getHomeInfoTask = new AsyncRequest(getActivity(), sender,
				"GET");
		getHomeInfoTask.execute(url);
	}

	private void addClickListenerButton() {

		buttonCall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Toast.makeText(getActivity(), "Calling", Toast.LENGTH_SHORT)
						.show();
				// callService(AppConstant.PHONE_CAR_STORE);
				callService(companyNumber);

			}
		});
		buttonEmail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Toast.makeText(getActivity(), "Email", Toast.LENGTH_SHORT)
						.show();
				emailService();

			}
		});
		buttonRoadAssist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// callService(AppConstant.PHONE_ROAD_ASSIST);
				callService(roadAssitanceNumber);

			}
		});

		imageBanner.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// callService(AppConstant.PHONE_CAR_STORE);
				callService(companyNumber);
			}
		});
		btnPlay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// callService(AppConstant.PHONE_CAR_STORE);
				new YourAsyncTask().execute();
				btnPlay.setVisibility(View.GONE);
			}
		});

	}

	protected void emailService() {

		UserInfoModel user = userPrefs.getUserInfo(UserInfoUtil.KEY_USER);
		if (!user.getVehicleNum().equals("")) {

			String emailTemplete = String.valueOf("VIN: "
					+ user.getVehicleNum() + "+\nCustomer Name: "
					+ user.getName() + "\nCustomer phone number(HOME): "
					+ user.getPhoneHome() + "\nCustomer phone number(MOBILE): "
					+ user.getPhoneMobile()
					+ "\nCustomer phone number(BUSINESS): "
					+ user.getPhoneBusiness() + "\nDrop off required:  "
					+ user.getServiceOptionStatus()
					+ "\nRepair completion text required: "
					+ user.getServiceOptionRepair());

			Intent email = new Intent(Intent.ACTION_SEND);
			// email.putExtra(Intent.EXTRA_EMAIL,
			// new String[] { getString(R.string.text_email_to) });
			email.putExtra(Intent.EXTRA_EMAIL, new String[] { companyEmail });
			// email.putExtra(Intent.EXTRA_CC, new String[]{ to});
			email.putExtra(Intent.EXTRA_BCC,
					new String[] { getString(R.string.text_email_bcc) });
			email.putExtra(Intent.EXTRA_SUBJECT,
					getString(R.string.text_email_sub));

			email.putExtra(Intent.EXTRA_TEXT, emailTemplete);

			// need this to prompts email client only
			email.setType("message/rfc822");

			startActivity(Intent.createChooser(email,
					"Choose an Email client :"));

		} else {
			com.dealernx.bavaria.bmw.util.Log.showAlertDialog(getActivity(),
					"User Detail",
					"Please setup your user detail on setting page first",
					false);
		}

	}

	public void callService(String number) {

		String uri2 = "tel:" + number;
		Intent intent2 = new Intent(Intent.ACTION_CALL);
		intent2.setData(Uri.parse(uri2));
		startActivity(intent2);
	}

	public void updatUiItem(CompanyModel model) {

		Picasso.with(getActivity()).load(model.getImageBanner())
				.resize(400, 200).centerCrop().into(imageBanner);

		companyNumber = model.getCompanyCall();
		companyEmail = model.getEmail();
		roadAssitanceNumber = model.getRsAssistNumber();

		uHandler.setCompanyInfo(model);
		addClickListenerButton();

	}

	private void setupVideoVideoComponent() {
		// TODO Auto-generated method stub

	}

	@Override
	public void asyncResponse(int request, String response) {
		CompanyModel model = JsonParser.getCompanyHome(response);
		if (model != null) {
			updatUiItem(model);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		// we use onSaveInstanceState in order to store the video playback
		// position for orientation change
		savedInstanceState.putInt("Position", videoView.getCurrentPosition());
		videoView.pause();
	}

	private class YourAsyncTask extends AsyncTask<Void, Void, Void> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(getActivity(), "",
					"Loading Video wait...", true);
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String url = "http://www.youtube.com/watch?v=0oRxXPGdxOs";
				videoUrl = getUrlVideoRTSP(url);
				Log.e("Video url for playing=========>>>>>", videoUrl);
			} catch (Exception e) {
				Log.e("Login Soap Calling in Exception", e.toString());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			/*
			            videoView.setVideoURI(Uri.parse("rtsp://v4.cache1.c.youtube.com/CiILENy73wIaGQk4RDShYkdS1BMYDSANFEgGUgZ2aWRlb3MM/0/0/0/video.3gp"));
			            videoView.setMediaController(new MediaController(AlertDetail.this));
			            videoView.requestFocus();
			            videoView.start();*/
			videoView.setVideoURI(Uri.parse(videoUrl));
			Bitmap thumb = ThumbnailUtils.createVideoThumbnail(videoUrl,
					MediaStore.Images.Thumbnails.MINI_KIND);
			// videoView.setb
			MediaController mc = new MediaController(getActivity());
			videoView.setMediaController(mc);
			videoView.requestFocus();
			videoView.start();
			mc.show();
		}

	}

	public static String getUrlVideoRTSP(String urlYoutube) {
		try {
			String gdy = "http://gdata.youtube.com/feeds/api/videos/";
			DocumentBuilder documentBuilder = DocumentBuilderFactory
					.newInstance().newDocumentBuilder();
			String id = extractYoutubeId(urlYoutube);
			URL url = new URL(gdy + id);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			Document doc = documentBuilder.parse(connection.getInputStream());
			Element el = doc.getDocumentElement();
			NodeList list = el.getElementsByTagName("media:content");// /media:content
			String cursor = urlYoutube;
			for (int i = 0; i < list.getLength(); i++) {
				Node node = list.item(i);
				if (node != null) {
					NamedNodeMap nodeMap = node.getAttributes();
					HashMap<String, String> maps = new HashMap<String, String>();
					for (int j = 0; j < nodeMap.getLength(); j++) {
						Attr att = (Attr) nodeMap.item(j);
						maps.put(att.getName(), att.getValue());
					}
					if (maps.containsKey("yt:format")) {
						String f = maps.get("yt:format");
						if (maps.containsKey("url")) {
							cursor = maps.get("url");
						}
						if (f.equals("1"))
							return cursor;
					}
				}
			}
			return cursor;
		} catch (Exception ex) {
			Log.e("Get Url Video RTSP Exception======>>", ex.toString());
		}
		return urlYoutube;

	}

	protected static String extractYoutubeId(String url)
			throws MalformedURLException {
		String id = null;
		try {
			String query = new URL(url).getQuery();
			if (query != null) {
				String[] param = query.split("&");
				for (String row : param) {
					String[] param1 = row.split("=");
					if (param1[0].equals("v")) {
						id = param1[1];
					}
				}
			} else {
				if (url.contains("embed")) {
					id = url.substring(url.lastIndexOf("/") + 1);
				}
			}
		} catch (Exception ex) {
			Log.e("Exception", ex.toString());
		}
		return id;
	}
}
