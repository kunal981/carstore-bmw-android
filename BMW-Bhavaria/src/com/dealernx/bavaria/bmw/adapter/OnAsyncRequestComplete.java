package com.dealernx.bavaria.bmw.adapter;

public interface OnAsyncRequestComplete {
	public void asyncResponse(int request, String response);
}
