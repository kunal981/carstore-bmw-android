package com.dealernx.bavaria.bmw.ws;

import java.io.IOException;

import android.util.Log;

import com.dealernx.bavaria.bmw.model.CompanyModel;
import com.dealernx.bavaria.bmw.parser.JsonParser;
import com.dealernx.bavaria.bmw.ws.WSConstant.PARAM;
import com.dealernx.bavaria.bmw.ws.WSConstant.VALUES;

public class WSConnector {

	public static final String TAG = WSConnector.class.getSimpleName();

	public static CompanyModel getHomeInfo() {
		String url = null;
		url = WSConstant.API_GET + PARAM.ACTION + "=" + VALUES.ACTION_HOME
				+ "&" + PARAM.COMPANY_APP_ID + "=" + VALUES.COMPANY_APP_ID;
		String jString = null;
		try {
			jString = WSAdapter.getJSONObject(url);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return JsonParser.getCompanyHome(jString);

	}

}
