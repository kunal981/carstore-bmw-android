package com.dealernx.bavaria.bmw.ws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import android.util.Log;

public class WSAdapter {

	public static final String TAG = WSAdapter.class.getSimpleName();
	public static final int HTTP_SESSION_TIME_OUT = (int) (30 * 1000);

	public static String getJSONObject(String url) throws IOException {
		InputStream iStream = null;
		String jString = null;

		HttpResponse response = null;
		// Making HTTP request
		try {
			// defaultHttpClient
			HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams,
					HTTP_SESSION_TIME_OUT);
			HttpConnectionParams
					.setSoTimeout(httpParams, HTTP_SESSION_TIME_OUT);
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url);

			response = httpclient.execute(httpGet);
			HttpEntity entity = response.getEntity();
			iStream = entity.getContent();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e(TAG, "" + e.getMessage());
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					iStream, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "");
			}
			jString = sb.toString();
			Log.d(TAG + "::Json String Response", jString);
		} catch (Exception e) {
			Log.e(TAG + "Buffer Error",
					"Error converting result " + e.toString());
		} finally {
			if (iStream != null) {
				iStream.close();
			}

		}
		return jString;

	}

	public static String postJSONObject(String url,
			List<NameValuePair> namevaluepairs) throws IOException {
		InputStream iStream = null;
		String jString = null;

		HttpResponse response = null;
		try {
			// defaultHttpClient
			HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams,
					HTTP_SESSION_TIME_OUT);
			HttpConnectionParams
					.setSoTimeout(httpParams, HTTP_SESSION_TIME_OUT);
			HttpClient httpclient = new DefaultHttpClient(httpParams);
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(new UrlEncodedFormEntity(namevaluepairs));
			// HttpResponse response = null;
			response = httpclient.execute(httpPost);

			HttpEntity entity = response.getEntity();
			iStream = entity.getContent();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					iStream, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "");
			}
			iStream.close();

			// Log.d("value response", sb.toString());
			jString = sb.toString();
			Log.d(TAG + "::Json String Response", jString);

		} catch (Exception e) {
			Log.e(TAG + "Buffer Error",
					"Error converting result " + e.toString());
		} finally {
			iStream.close();

		}
		return jString;

	}

}
