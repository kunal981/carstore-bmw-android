package com.dealernx.bavaria.bmw.controller;

import com.dealernx.bavaria.bmw.model.CompanyModel;

public class UiHandler {

	public static UiHandler uInstance;

	public CompanyModel companyInfo;

	/* Static 'instance' method */
	public static UiHandler getInstance() {
		return uInstance;
	}

	public static void initInstance() {
		if (uInstance == null) {
			uInstance = new UiHandler();
		}
	}

	public CompanyModel getCompanyInfo() {
		return companyInfo;
	}

	public void setCompanyInfo(CompanyModel companyInfo) {
		this.companyInfo = companyInfo;
	}

}